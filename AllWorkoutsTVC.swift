//
//  AllWorkoutsTVC.swift
//  Workout App
//
//  Created by Silvia Florido on 2/22/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

class AllWorkoutsTVC: CoreDataTVC, UISearchResultsUpdating {
    
    var workouts: [Workout]?
    //var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
    var context: NSManagedObjectContext! {  didSet {   initFetchedResultsController()  }  }
    
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredWorkouts = [Workout]()
    
    
    
   // var frc: NSFetchedResultsController<NSFetchRequestResult>!
    
    func initFetchedResultsController(){
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        request.predicate = NSPredicate(format: "isActive == %@ ", NSNumber(booleanLiteral: true))
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                         managedObjectContext: context,
                                         sectionNameKeyPath: nil,
                                         cacheName: nil)
    }
    
    
    // MARK: -  Search Controller
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredWorkouts = workouts!.filter { workout in
            return (workout.name?.lowercased().contains(searchText.lowercased()))!
        }
    
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell", for: indexPath)

        // Configure the cell...

   
        guard let workout = fetchedResultsController?.object(at: indexPath) as? Workout else { return cell }
            
            //fatalError("Unexpected Object in FetchedResultsController")
        
        
        cell.textLabel?.text = workout.name

        
        if let exercises = workout.exercises?.array as? Array<Exercise> {
            
            var setsCount = 0
            
            for exercise in exercises {
                if let sets = exercise.sets {
                    setsCount += sets.count
                }
            }
            
            cell.detailTextLabel?.text = "\(exercises.count) Exercises, \(setsCount) Sets"
        }
        
        return cell
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            // Delete the row from the data source
            
            //tableView.deleteRows(at: [indexPath], with: .fade)
            
            // Delete object in model and let fetchedResultsController sync the table
            let workoutToDelete = fetchedResultsController?.object(at: indexPath) as! NSManagedObject
            
            context.delete(workoutToDelete)
            saveContext()
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        
        
    }
    

    
    func saveContext() {
        context.perform{
            do {
                try self.context.save()
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "Show Workout" {
            let destination = segue.destination as! WorkoutTVC
            destination.workout = fetchedResultsController?.fetchedObjects![tableView.indexPathForSelectedRow!.row] as! Workout
                
                //fetchedResultsController?.fetchedObjects[tableView.indexPathForSelectedRow!.row] as! Workout
            destination.context = context
        }
        
    }
    

}










