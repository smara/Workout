//
//  CreateWorkoutViewController.swift
//  Workout App
//
//  Created by Silvia Florido on 10/26/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//


/** Development notes

    Validate name when textField didEndEditing and show error OR thinking about presenting a modal view asking the user if it really wants to keep the repeated name.  Oct 26, 2016
 
    Decided to colect all data and create the workout after Next is pressed to avoid problems with unique name DB checking and autosaving to soon. Oct 26, 2016
 
    Next button will prepareForSegue OR will save the workout and then performSegue programatically ? 
    Another option is to leave it to prepareForSegue only and implement "validate name not nil", "create workout" and "segue". In this case, remove shouldPerformSegue.  Oct 26, 2016
 **/

import UIKit
import CoreData


class CreateWorkoutViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var workoutNameTxt: UITextField!
    @IBOutlet weak var frequenceSegments: MultiSelectSegmentedControl!
    @IBOutlet weak var nameWarningLabel: UILabel!
    
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var frequence: String {
        get{
            let frequenceArray = frequenceSegments.selectedSegmentTitles as! Array<String>
            return frequenceArray.joined(separator: ",")
        }
    }
    
    // workout name field - size and validation - show icon to fast-delete textfield (x) like the Apple Contacts App
    
    // scroll fields to visible area when keyboard is visible
    
    // actions - save / next / more options
    
    // autocomplete/search name. Show list from the begining to help the user choose a pré-existing name.
    
    // add "delete field" to name as in Apple Contacts App
    
    

    
    @IBAction func nextButton(_ sender: UIButton) {
       
        view.endEditing(true)
        validateWorkoutName()
        //get name and check if not empty and unique in DB, if so:
       
        //create workout Obj
        let workout = Workout.createWorkout(inManagedObjectContext: context)
        workout.name = workoutNameTxt.text!
        workout.frequence = frequence
        
        // proceed to next screen (call performSegue)
    
    }
    
    /*
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        // validate that workout has a name at least, frequence may be optional
        return false
    }
    */
    
    
    
    
    
    // MARK: UITextFieldDelegate
    
    // dismis keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    // dismiss keyboard when view touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == workoutNameTxt {   validateWorkoutName()   }
    }
    
    @IBAction func selectedSegmentedDidChange(_ sender: MultiSelectSegmentedControl) {
        // dismiss keyboard if visible and call textFieldDidEndEditing for name validation
        view.endEditing(true)
        print(sender.selectedSegmentTitles)
    }

   
    func validateWorkoutName() {
        // enforce that the text is not empty ("")
        guard workoutNameTxt.text?.isEmpty == false else {
            return
        }
        
        if let newName = workoutNameTxt.text?.trimmingCharacters(in: CharacterSet.whitespaces) {
            // validate if name is unique in DB
        }else {
            // Maybe replace this animation with a modal view asking if we should keep the name even though it is not unique.
            UIView.animate(withDuration: 1,
                           delay: 0,
                           options: .curveEaseOut,
                           animations: {self.nameWarningLabel.alpha = 1},
                           completion: { (Bool) -> Void in
                            UIView.animate(withDuration: 1,
                                           delay: 2,
                                           options: .curveEaseIn,
                                           animations: {self.nameWarningLabel.alpha = 0},
                                           completion: nil)
            })
            print("Já existe um treino com esse nome! Por favor escolha outro.")
        }

    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}








