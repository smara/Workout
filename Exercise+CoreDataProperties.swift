//
//  Exercise+CoreDataProperties.swift
//  Workout App
//
//  Created by Silvia Florido on 10/30/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import Foundation
import CoreData


extension Exercise {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exercise> {
        return NSFetchRequest<Exercise>(entityName: "Exercise");
    }

    @NSManaged public var name: Bool
    @NSManaged public var sets: NSSet?
    @NSManaged public var workout: Workout?

}

// MARK: Generated accessors for sets
extension Exercise {

    @objc(addSetsObject:)
    @NSManaged public func addToSets(_ value: SetSerie)

    @objc(removeSetsObject:)
    @NSManaged public func removeFromSets(_ value: SetSerie)

    @objc(addSets:)
    @NSManaged public func addToSets(_ values: NSSet)

    @objc(removeSets:)
    @NSManaged public func removeFromSets(_ values: NSSet)

}
