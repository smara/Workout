//
//  AddExerciseCVC.swift
//  Workout App
//
//  Created by Silvia Florido on 3/7/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData


class AddExerciseCVC: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {

    @IBOutlet weak var exerciseCollectionView: UICollectionView!
    
    var context: NSManagedObjectContext!
    var workout: Workout! {
        didSet { context = workout.managedObjectContext }
    }
    private var selectedSetByExerciseDict = [Int:Int]()    //  [Exercise:Set]
    var exercisesArray: Array<Exercise>? { get { return  workout?.exercises?.array as! Array<Exercise>? }   }
    

    
    // MARK: UICollectionViewDataSource
     override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return workout.exercises?.count ?? 0
    }
    
    
     override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let exerciseCell = collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell", for: indexPath) as! ExerciseCollectionViewCell

        let selectedSetNumber = selectedSetByExerciseDict[indexPath.item] ?? 0
        
        
         // populate cell / exercise data
         let exercise = exercisesArray![indexPath.item]
         exerciseCell.exerciseNameTxt.text = exercise.name
         exerciseCell.exercise = exercise
         
        // set data
        if let set = exercise.sets?.array[selectedSetNumber] as? SetSerie {
         exerciseCell.setNumberLabel.text = "Set \(set.number)"
         exerciseCell.repsTxt.text = String(set.reps)
         exerciseCell.weightTxt.text = String(set.weight)
         }
        
        // keeps selected set state by exercise cell
        if exerciseCell.setsCollectionView.cellForItem(at: IndexPath(item: selectedSetNumber, section:0)) != nil {
            exerciseCell.setsCollectionView.selectItem(at: IndexPath(item: selectedSetNumber, section:0), animated: false, scrollPosition: .left)
        }
        return exerciseCell
    }

    
    
    // MARK: UICollectionViewDelegate
    
     override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let exerciseCell = cell as! ExerciseCollectionViewCell
        exerciseCell.exerciseNameTxt.delegate = exerciseCell
        exerciseCell.repsTxt.delegate = exerciseCell
        exerciseCell.weightTxt.delegate = exerciseCell
        
    }
    
    
    // keeps selected set state by exercise
//     override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if let exerciseCell = cell as? ExerciseCollectionViewCell {
//            let exerciseNumber = indexPath.item
//            let setNumber = exerciseCell.setsCollectionView.indexPathsForSelectedItems![0].item
//            selectedSetByExerciseDict[exerciseNumber] = setNumber
//        }
//    }
    
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
    
    // MARK: UICollectionViewFlowDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: view.frame.size.height - 70)
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        setupCollectionView()
        
        if workout.exercises?.count == 0 {
            // setup initial exercise and set
            if let exercise = Exercise.createExercise(inManagedObjectContext: context, in: workout) {
                if let set = SetSerie.createSet(inManagedObjectContext: context, for: exercise) {
                    set.number = Int16(exercise.sets!.count)
                    set.reps = 10
                    set.weight = 10
                    selectedSetByExerciseDict[0] = 0
                }
            }
        }
    }
    


     // MARK: - Navigation
     
     // finish button
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        view.endEditing(true)
        
        if segue.identifier == "Save Workout" {
            saveContext()
            if let destinationVC = segue.destination as? WorkoutTVC {
                destinationVC.workout = workout
            }
        } else {  // Cancel unwind button
            context.delete(workout)
        }
        
    }

    
    
    // MARK: Actions
    @IBAction func AddNextExerciseButton(_sender: UIButton) {
        
        // save current data ? - done in textfield delegates
        
        // create exercise in model
        if let exercise = Exercise.createExercise(inManagedObjectContext: context, in: workout) {
            // one initial set is always created with the exercise
            if let set = SetSerie.createSet(inManagedObjectContext: context, for: exercise) {
                set.number = Int16(exercise.sets!.count)
                set.reps = 10
                set.weight = 10
                selectedSetByExerciseDict[0] = 0
            }
            
            // add to Exercises Collection
            let newIndexPath = IndexPath(item: exerciseCollectionView.numberOfItems(inSection: 0), section: 0)
            exerciseCollectionView.insertItems(at: [newIndexPath])
            
            // scroll to new Exercise
            exerciseCollectionView.scrollToItem(at: newIndexPath, at: .left, animated: true)
        } 
        
    }
    
    @IBAction func scroll(_ sender: UIButton) {
        //  scrollToExerciseAt(index: 5
        let scrollView = exerciseCollectionView as UIScrollView
        if sender.titleLabel?.text == "Next" {
            scrollView.contentOffset.x += view.frame.size.width//exerciseCollectionView!.contentSize.width/CGFloat(workout.exercises!.count)
        } else {
            scrollView.contentOffset.x -= view.frame.size.width//exerciseCollectionView!.contentSize.width/CGFloat(workout.exercises!.count)
        }
    }
    
    // MARK: Helpers
    private func setupCollectionView(){
        if let layout = exerciseCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 0
        }
        exerciseCollectionView?.isPagingEnabled = true
        // collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    func saveContext(){
        context.performAndWait {
            do {
                try self.context.save()
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    

    

}





















