////
////  AddExerciseAllTogetherCellCollectionVC.swift
////  Workout App
////
////  Created by Silvia Florido on 11/20/16.
////  Copyright © 2016 Silvia Florido. All rights reserved.
////
//
//import UIKit
//import CoreData
//
//private let reuseIdentifier = "Cell"
//
//
//class AddExerciseCollectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UITextFieldDelegate {
//    
//    @IBOutlet weak var exerciseCollectionView: UICollectionView!
//    @IBOutlet weak var AddToSetButton: UIButton!
//    
//    var context: NSManagedObjectContext!   // yes, crash if nil
//    var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
//    
//    var editionMode: Bool = false
//    var selectedSet: SetSerie?
//    
//    var exercisesArray: Array<Exercise>? { get { return  workout?.exercises?.array as! Array<Exercise>? }   }
//    
//    var exerciseForThisCell: Exercise!
//    
//    lazy var tvc: SearchTableView = SearchTableView()
//
//    var sequence: [String:[Int]] = ["A":[], "B":[], "C":[], "D":[], "E":[]]
//    
//    //MARK: - Actions
//    
//    @IBAction func addAnotherSetButton(_ sender: UIButton) {
//        if let exerciseCell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
//            
//            view.endEditing(true)
//            // reps need to be > 0
//            //  guard !(cell.repsTxt.text!.isEmpty) && (Int16(cell.repsTxt.text!)! > 0) else { return }
//            
//            //exerciseCell.isSelected = true  // select the ExerciseCell because you just cliked on its button
//            
//            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
//            exerciseForThisCell = exercisesArray![exerciseIndex]
//            var setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
//            
//            
//            
//            // save current set, selected or last
//            // NEW
//            if let selectedSet = exerciseCell.setsCollectionView, let sel = selectedSet.indexPathsForSelectedItems, sel.count > 0 {
//                
//                let selectedSetIndex = sel[0].item
//            //if let selectedSetIndex = exerciseCell.setsCollectionView.indexPathsForSelectedItems?[0].item {
//                let selectedSetItem = setsForThisExercise[selectedSetIndex]
//                saveCurrentSet(selectedSetItem, for: exerciseCell)
//            } else {
//                saveCurrentSet(setsForThisExercise.last!, for: exerciseCell)
//            }
//            
//            
//            /*
//            if selectedSet != nil {
//                saveCurrentSet(selectedSet!, for: exerciseCell)
//            } else {
//                saveCurrentSet(setsForThisExercise.last!, for: exerciseCell)
//            }
//             */
//            
//            
//            // create a new set for this exercise
////            if let newSet = createSet(for: exerciseForThisCell) {
//            
//                
//               // print("Added another Set: \(newSet.number) for Exercise: \(newSet.exercise?.name)")
//               // print("")
//                
//                
////                // Use last Set data for ease of typing. This method will never be called for first set.
////                setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
////                let previousSet = setsForThisExercise[setsForThisExercise.count - 2]
////                
////                newSet.reps = previousSet.reps
////                newSet.weight = previousSet.weight
////                
//                // new version mar17 - reload new set index only - Will Not work (08-mar-17)
//                //exerciseCell.setsCollectionView.reloadItems(at: IndexPath(item: setNumber, section: exerciseNumber)
//    
////                exerciseCell.setsCollectionView.insertItems(at: [IndexPath(item: setsForThisExercise.count - 1, section: 0) ])
////                //exerciseCell.setsCollectionView.reloadData()
//                
////                // scroll to show newSet
////                let newSetIndexPath = IndexPath(item: (setsForThisExercise.index(of: newSet))!, section: 0)
////                exerciseCell.setsCollectionView.scrollToItem(at: newSetIndexPath, at: .left, animated: true)
////                
////                //selects last set cell (appearance/highlighting defined in cell subclass)
////                let lastSetIndex = IndexPath(item: (setsForThisExercise.count - 1), section: 0)
////                exerciseCell.setsCollectionView.selectItem(at:  lastSetIndex, animated: true, scrollPosition: .left)
////                
//                
//
//               
////                
////            } else {
////                //error message
////            }
//        }
//    }
//    
//    
//    @IBAction func AddNextExerciseButton(_ sender: UIButton) {
//        if let exerciseCell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
//            
//            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
//            
//            // if last exercise then create new, otherwise just scroll
//            if exerciseIndex == (exercisesArray!.count - 1) {
//                exerciseForThisCell = exercisesArray![exerciseIndex]
//                
//                if sender == exerciseCell.addToSetButton {
//                    //TODO: create in db an attribute sequence (A,B,C)
//                }
//                
//                 //let setToSave = selectedSet ?? exerciseForThisCell.sets?.array.last as! SetSerie
//                //saveCurrentSet(setToSave, for: exerciseCell)
//                
//                
////                // NEW
////                let setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
////                
////                if let selectedSet = exerciseCell.setsCollectionView, let sel = selectedSet.indexPathsForSelectedItems, sel.count > 0 {
////                    
////                    let selectedSetIndex = sel[0].item
////                    let selectedSetItem = setsForThisExercise[selectedSetIndex]
////                    saveCurrentSet(selectedSetItem, for: exerciseCell)
////                } else {
////                    saveCurrentSet(setsForThisExercise.last!, for: exerciseCell)
////                }
//                
//                
//                
////                if let newExercise = createExercise() {   // creates new Set automatically
//////                    exerciseCollectionView.reloadData()
////                    
////                    let indexPath = IndexPath(item: (exercisesArray?.index(of: newExercise))!, section: 0)
////                    exerciseCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
////                } else {
////                    //error message
////                }
//            } else if exerciseIndex < (exercisesArray!.count - 1) {
//            
//                // user is re-viewing previously created exercises so just scroll to the next
//                let nextIndexPath = IndexPath(item: exerciseIndex + 1, section: 0)
//                exerciseCollectionView.scrollToItem(at: nextIndexPath, at: .left, animated: true)
//
//            }
//            
//        }
//    }
//    
//    
//    
//    
////    @IBAction func finishWorkout(_ sender: UIButton) {
////        
////        // cell that contains the pressed button
////        if let exerciseCell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
////            
////            // get the exercise related to this cell
////            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
////            exerciseForThisCell = exercisesArray![exerciseIndex]
////            
////          //  let setToSave = selectedSet ?? exerciseForThisCell.sets?.array.last as! SetSerie
////           // saveCurrentSet(setToSave, for: exerciseCell)
////            
////            // NEW
////            let setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
////            if let selectedSetIndex = exerciseCell.setsCollectionView.indexPathsForSelectedItems?[0].item {
////                let selectedSetItem = setsForThisExercise[selectedSetIndex]
////                saveCurrentSet(selectedSetItem, for: exerciseCell)
////            } else {
////                saveCurrentSet(setsForThisExercise.last!, for: exerciseCell)
////            }
////        }
////    }
////    
//    
//    
////    func createExercise() -> Exercise? {
////        if let exercise = Exercise.createExercise(inManagedObjectContext: context!) {
////            
////            workout.add(exercise: exercise)
////            
////            //exercise.number = Int16(exercisesArray!.count)
////            //exercise.name = "A " + String(exercise.number) + " - "
////            
////            _ = createSet(for: exercise)
////            return exercise
////        }
////        return nil
////    }
////    
//    
////    func createSet(for exercise:Exercise) -> SetSerie?{
////        if let newSet = SetSerie.createSet(inManagedObjectContext: context) {
////            
////            exercise.add(setSerie: newSet)
////            //exercise.addToSets(newSet)
////            newSet.number = Int16(exercise.sets!.count)
////            
////            // initial values for the FIRST Set only to make UI more friendly. Next Sets will use previouos set values.
////            if newSet.number == 1 {
////                newSet.reps = 10
////                newSet.weight = 10
////            }
////
////            
////            
////            return newSet
////        }
////        return nil
////    }
////    
//    
//    
//    
//    func saveCurrentSet(_ set: SetSerie , for cell: ExerciseCollectionViewCell){
//        // save current set with the editable values in the Exercise cell fields
//        
//        
//        
//        // TODO: this should be better. Maybe tell the user about the problem and ask what to do. Transfer this guard to
//        // one of the action (buttons pressed)
//        // reps need to be > 0
//        guard !(cell.repsTxt.text!.isEmpty) else { return }
//        //guard (Int16(exerciseCell.repsTxt.text!)! > 0) else {return}
//        
//        set.reps = Int16(cell.repsTxt.text!)!
//        set.weight = Int16(cell.weightTxt.text!) ?? 0
//    }
//    
//    
//    
//    
//    func collectionViewCell(for aView: UIView) -> UICollectionViewCell? {
//        var cell:UIView? = aView.superview
//        
//        while !(cell is UICollectionViewCell) && (cell != nil)  {
//            cell = cell!.superview
//        }
//        return cell as? UICollectionViewCell
//    }
//    
//    
//    
//    
//    func saveContext(){
//        context.performAndWait {
//            do {
//                try self.context.save()
//            } catch let error {
//                print("Core Data Saving Error: ", error)
//            }
//        }
//        
//    }
//    
//    
//    
//    
//    // MARK: - UITextFieldDelegate
//    
////    // dismis keyboard
////    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
////        textField.resignFirstResponder()
////        return true
////    }
////    
//    
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
////
////        if let exerciseCell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
////        
////            if textField == exerciseCell.exerciseNameTxt {
//                let searchText: NSString = NSString(string:textField.text!)
//                let resultingString = searchText.replacingCharacters(in: range, with: string)
////
////                
////                
////                // TODO: Make a better blur effect under the search table.
                /*  let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
                 
                 
                 effectView.frame = view.frame
                 effectView.frame.origin =  CGPoint(x: textField.frame.origin.x, y: (textField.frame.origin.y + textField.frame.size.height))
                 
                 //effectView.alpha = 0.6
                 exerciseCell.contentView.addSubview(effectView)
                 */
////
                // Search Table View
//                tvc.searchAutoCompleteEntries(with: resultingString, for: textField)
//                
//                exerciseCell.contentView.addSubview(tvc.tableView)
//                
//                //effectView.addSubview(tvc.tableView)
//                tvc.txtColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
////            }
////        }
////        return true
////    }
//    
//    
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        
//        if let exerciseCell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
//           
//            // update correct exercise and sets for THIS cell
//            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
//            exerciseForThisCell = exercisesArray![exerciseIndex]
//            let setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
//            
//            
//            // selects the correct set that is being updated
//            let setNumberString = exerciseCell.setNumberLabel.text!.replacingOccurrences(of: "Set ", with: "")
//            let setNumberInt = Int(setNumberString)! - 1
//            
//            let set = setsForThisExercise[setNumberInt]
//            selectedSet = set
//            
//            for cell in exerciseCollectionView.visibleCells {
//                let exerciseCell = cell as! ExerciseCollectionViewCell
//                let setsCV = exerciseCell.setsCollectionView!
//                
//           //     print("Exercise = \(exercisesArray?[(exerciseCollectionView.indexPath(for: exerciseCell)?.item)!].name!)")
//                print("Selected Set = \(setsCV.indexPathsForSelectedItems)")
//                
//                
//            }
//
//           // print("Selected Set = \(exerciseCell.setsCollectionView.indexPathsForSelectedItems)")
//           // print("Exercise = \(exercisesArray?[exerciseIndex].name)")
//
//
//           // exerciseCell.setsCollectionView.selectItem(at: , animated: true, scrollPosition: .left)
//
//            
//            
//            let index = IndexPath(item: setNumberInt, section: 0)
//            
//            
////            // TODO: make numbers be updated to the Sets when typed in TxtFields (maybe use textFieldShouldChange func)
////            
////            switch textField {
////            case exerciseCell.exerciseNameTxt:
////                exerciseForThisCell.name = textField.text
////                
////            case exerciseCell.repsTxt:
////                if exerciseCell.repsTxt.text == nil || exerciseCell.repsTxt.text == "0" || exerciseCell.repsTxt.text == "" {
////                    exerciseCell.repsTxt.text = "1"
////                }
////                set.reps = Int16(exerciseCell.repsTxt.text!)!
////                exerciseCell.setsCollectionView.reloadItems(at: [index])
////            case exerciseCell.weightTxt:
////                if exerciseCell.weightTxt.text == nil || exerciseCell.weightTxt.text == "" {
////                    exerciseCell.weightTxt.text = "0"
////                }
////                set.weight = Int16(exerciseCell.weightTxt.text!)!
////                exerciseCell.setsCollectionView.reloadItems(at: [index])
////            default:
////                break
////            }
//        }
//    }
//    
//    
//
//    
//    
//    // MARK: - UICollectionViewDataSource
//    
//    // This var should be used to mount the Sets Cells. Set it also when the view scrolls.
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        var items: Int = 0
//        
//        // Exercises Collection View
//        if collectionView == exerciseCollectionView {
//            items = workout.exercises?.count ?? 0
//            
//        } else {
//            
////            // Sets Collection View   --->   collectionView == setsCollectionView
////            if let setsForThisExercise = exerciseForThisCell.sets?.array.count {
////                items = (setsForThisExercise > 0) ? setsForThisExercise : 1
////            }
////        }
//        return items
//    }
//    
//    
//    
//    
//        
//        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//            
//            var cell: UICollectionViewCell!
//            
//            // Exercises Collection View
//            if collectionView == exerciseCollectionView {
//                
//                // dequeue cell
//                if let exerciseCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell" , for: indexPath)  as? ExerciseCollectionViewCell {
//                    
//                    // var to help mount and update Set cells
//                    exerciseForThisCell = exercisesArray![indexPath.item]
//                    
//                    
//                    // if I have sets for this exercise, get them
//                    if let setsForThisExercise = exerciseForThisCell.sets?.array as? Array<SetSerie> {
//                        
//                        
//                        // show selectedSet if any or last set
//                        // let set = selectedSet ?? (setsForThisExercise.last!)
//                        
//                        var set: SetSerie!
//                        // NEW
//                        // let test = exerciseCell.setsCollectionView.indexPathsForSelectedItems
//                        if let selectedSet = exerciseCell.setsCollectionView, let sel = selectedSet.indexPathsForSelectedItems, sel.count > 0 {
//                            let selectedSetIndex = sel[0].item
//                            set = setsForThisExercise[selectedSetIndex]
//                        } else {
//                            set = setsForThisExercise.last!
//                        }
//                        
//                        
//                        
//                        // Populate Exercise cell
//                        exerciseCell.exerciseNameTxt.text = exerciseForThisCell.name
//                        exerciseCell.exercise = exercisesArray![indexPath.item]
//                        
//                        
//                        // sets values
//                        //exerciseCell.setNumberLabel.text = String(set.number)
//                        exerciseCell.repsTxt.text = String(set.reps)
//                        exerciseCell.weightTxt.text = String(set.weight)
//                    }
//                    
//                    
//                    // Currently showing Next button always but only last exercise creates another. For the other exercises, the button just scrolls (Mar,1 17)
//                    // show AddExercise button only for the last exercise cell
//                    /*
//                     if exerciseForThisCell == exercisesArray?.last {
//                     exerciseCell.addExerciseButton.isHidden = false
//                     } else {
//                     exerciseCell.addExerciseButton.isHidden = true
//                     }
//                     */
//                    
//                    cell = exerciseCell
//                }
//            } else {
//                
//                //  Sets Collection View
//                
//                if let setCell = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as? SetCollectionViewCell {
//                    
//                    // pointer to to exerciseCell which this setsCV belongs to
//                    if let exerciseCell = collectionViewCell(for: collectionView) as? ExerciseCollectionViewCell {
//                        
//                        // if I have sets for this exercise, get them
//                        if let setsForThisExercise = exerciseForThisCell.sets?.array as? Array<SetSerie> {
//                            
//                            if (setsForThisExercise.count) > indexPath.item {
//                                
//                                let set = setsForThisExercise[indexPath.item]
//                                
//                                //   print("")
//                                //  print("Cell for set: \(set.number) for Exercise: \(exerciseForThisCell.name) at index: \(indexPath.item)")
//                                //  print("")
//                                
//                                // Populate Set cell
//                                setCell.setNumber.text = String(set.number)
//                                setCell.reps.text = String((set.reps))
//                                setCell.weight.text = String(set.weight)
//                                
//                                
//                                // first cell wasn't created by addAnotherSet and was not selected.
//                                if setsForThisExercise.count == 1 {
//                                    
//                                    collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
//                                    
//                                    for cell in exerciseCollectionView.visibleCells {
//                                        let exerciseCell = cell as! ExerciseCollectionViewCell
//                                        let setsCV = exerciseCell.setsCollectionView!
//                                        
//                                        print("Exercise = \(exercisesArray?[(exerciseCollectionView.indexPath(for: exerciseCell)?.item)!].name)")
//                                        print("Selected Set = \(setsCV.indexPathsForSelectedItems)")
//                                        
//                                        
//                                    }
//                                    
//                                    //  print("Selected Set = \(exerciseCell.setsCollectionView.indexPathsForSelectedItems)")
//                                    //  print("Exercise = \(exercisesArray?[(exerciseCollectionView.indexPath(for: exerciseCell)?.item)!].name)")
//                                    
//                                    
//                                    setCell.isSelected = true
//                                    selectedSet = set
//                                }
//                                
//                                
//                                // TODO: Improve highlighting appearence, maybe using a background image with shadow or gradient
//                                // cell appearance
//                                if selectedSet == set {
//                                    let views = setCell.subviews[0].subviews
//                                    for view in views {
//                                        if let label = view as? UILabel {
//                                            label.textColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
//                                        }
//                                    }
//                                } else {
//                                    let views = setCell.subviews[0].subviews
//                                    for view in views {
//                                        if let label = view as? UILabel {
//                                            label.textColor = UIColor(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
//                                        }
//                                    }
//                                }
//                                
//                                
//                                // update the set number in the Exercise cell because when we createAnotherSet we don't reload the ExercisesCV
//                                // let setNumber = selectedSet ?? (setsForThisExercise.last!)
//                                var setNumber: SetSerie!
//                                
//                                // NEW
//                                
//                                if let selectedSet = exerciseCell.setsCollectionView, let sel = selectedSet.indexPathsForSelectedItems, sel.count > 0 {
//                                    
//                                    let selectedSetIndex = sel[0].item
//                                    setNumber = setsForThisExercise[selectedSetIndex]
//                                } else {
//                                    setNumber = setsForThisExercise.last!
//                                }
//                                
//                                
//                                exerciseCell.setNumberLabel.text = "Set " + String(setNumber.number)
//                                exerciseCell.setNumberLabel.textColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
//                                
//                                
//                                cell = setCell
//                            }
//                        }
//                    }
//                }
//                //   print("Return Set Cell")
//            }
//            return cell
//        }
//        
//    
//    
//    
//    
//    
//    // MARK: - UICollectionViewDelegate
//    
//        
//        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//            
//            guard collectionView != exerciseCollectionView else { return }
//            
//            // get the right exercise for this cell
//            let exerciseCell = collectionViewCell(for: collectionView) as! ExerciseCollectionViewCell
//            let exerciseIndex = exerciseCollectionView.indexPath(for: exerciseCell)!
//            exerciseForThisCell = exercisesArray?[exerciseIndex.item]
//            
//            
//            exerciseCell.setsCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
//            
//            /*
//             for cell in exerciseCollectionView.visibleCells {
//             let exerciseCell = cell as! ExerciseCollectionViewCell
//             let setsCV = exerciseCell.setsCollectionView!
//             
//             print("Exercise = \(exercisesArray?[(exerciseCollectionView.indexPath(for: exerciseCell)?.item)!].name!)")
//             print("Selected Set = \(setsCV.indexPathsForSelectedItems)")
//             
//             
//             }
//             */
//            //print("Selected Set = \(exerciseCell.setsCollectionView.indexPathsForSelectedItems)")
//            //print("Exercise = \(exercisesArray?[exerciseIndex.item].name)")
//            
//            
//            let sets = exerciseForThisCell.sets?.array as! Array<SetSerie>
//            selectedSet = sets[indexPath.item]
//            
//            exerciseCollectionView.reloadItems(at:[exerciseCollectionView.indexPath(for: exerciseCell)!])
//            
//        }
//    
//    
//    
//    /*
//     // Uncomment this method to specify if the specified item should be highlighted during tracking
//     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
//     return true
//     }
//     */
//    
//    /*
//     // Uncomment this method to specify if the specified item should be selected
//     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//     return true
//     }
//     */
//    
//    /*
//     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
//     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
//     return false
//     }
//     
//     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
//     return false
//     }
//     
//     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
//     
//     }
//     */
//    
//    
//    /*
//    // MARK: UICollectionViewFlowLayoutDelegate
//    
//    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return   CGSize(width: view.frame.size.width, height: (view.frame.size.height - collectionView.contentInset.bottom - collectionView.contentInset.top))
//    }
//     */
//    
//    
//    // MARK: - Navigation
//    
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
////    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
////        // Get the new view controller using [segue destinationViewController].
////        // Pass the selected object to the new view controller.
////        
////        
////        view.endEditing(true)   // dismiss keyboard
////        
////        if segue.identifier == "Save Workout" {
////            saveContext()
////            
////            if let destinationVC = segue.destination as? WorkoutTVC {
////                destinationVC.workout = workout
////                destinationVC.context = context
////            }
////        } else {   // cancel workout creation
////           context.delete(workout)
////        }
////    
////    }
//
//    
//    
//    // MARK: - Life cycle
//    
//    override func viewWillAppear(_ animated: Bool) {
//       
//        /*
//
//        exerciseCollectionView.reloadData()
//        
//        let count = exercisesArray!.count
//        
//        for index in 0...(count - 1) {
//            let indexPath = IndexPath(item: index, section: 0)
//            if let exerciseCell = exerciseCollectionView.cellForItem(at: indexPath) as? ExerciseCollectionViewCell{
//                exerciseCell.setsCollectionView.reloadData()
//                
//            }
//        }
//        */
//        
//    }
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Uncomment the following line to preserve selection between presentations
//        // self.clearsSelectionOnViewWillAppear = false
//        
////        setupCollectionView()
////        
////        if workout.exercises?.count == 0 {
////            _ = createExercise()  // creates the first Set automatically
////        // what if I can't get an exercise and/or set here ? Back to previous ViewController ?
////        }
//        
//        
//              
//    }
//    
//    
//    func setupCollectionView(){
//        if let layout = exerciseCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.minimumLineSpacing = 0
//            
//        }
//        
//        exerciseCollectionView?.isPagingEnabled = true
//        exerciseCollectionView.allowsMultipleSelection = false
//        exerciseCollectionView.reloadData()
//
//        
//        // collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
//    }
//
//    
//   
//    
//    
//}
//
//
//
//
//
//
