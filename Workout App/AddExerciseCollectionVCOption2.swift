//
//  AddExerciseCollectionVC.swift
//  Workout App
//
//  Created by Silvia Florido on 11/5/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "Cell"

class AddExerciseCollectionVCOption2: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var exerciseCollectionView: UICollectionView!
    @IBOutlet weak var setsCollectionView: UICollectionView!
    
    var context: NSManagedObjectContext!   // yes, crash if nil
    var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
    
    var exercise: Exercise?
    var newSet: SetSerie?
    
    var setNumber: Int16 = 0
    var setNumberAsString: String {   get { return String(setNumber)}   }
    
    var sets: Array<SetSerie>?   // for Collection View
    
    //var selectedItemIndexPath: IndexPath?

    var visibleExerciseCellIndex: IndexPath?
    
    //MARK: Actions
    
    @IBAction func addAnotherSetButton(_ sender: UIButton) {
       
        
        if let cell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {

            // reps need to be > 0
            guard !(cell.repsTxt.text!.isEmpty) && (Int16(cell.repsTxt.text!)! > 0) else { return }
            
            saveCurrentSet(for: cell)
            
            // zero the set fields
            cell.repsTxt.text = ""
            cell.weightTxt.text = ""
            cell.setNumberLabel.text = "Set " + setNumberAsString
        }
        
                // create a new set
        createSet()
        updateExercisesCollectionView()
        updateSetsCollectionView()
        
    }
    
    @IBAction func AddNextExerciseButton(_ sender: UIButton) {
        let cell = exerciseCollectionView.visibleCells.first as! ExerciseCollectionViewCell
        //cell.exerciseNameTxt.text = ""
        
        //let cell = sender.superview as! ExerciseCollectionViewCell
        //cell.exerciseNameTxt.text = ""
        //exercise?.name = ""
        
        saveCurrentSet(for: cell)
        
        setNumber = 0
        createExercise()
        createSet()
        //exerciseCollectionView.reloadItems(at: <#T##[IndexPath]#>)
        updateExercisesCollectionView()
        updateSetsCollectionView()
    }
    
    
    func createExercise(){
        exercise = Exercise.createExercise(inManagedObjectContext: context!)
        workout.addToExercises(exercise!)
        print("Exercise created! Workout contains \(workout.exercises?.count) exercises")
    }
    
    func createSet(){
        if let newSet = SetSerie.createSet(inManagedObjectContext: context) {
            self.newSet = newSet
            setNumber += 1
            newSet.number = setNumber
            exercise?.addToSets(newSet)
        }
    }
    
    func saveContext(){
        context.perform{
            do {
                try self.context.save()
                print("Exercise Saved.")
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    
    func saveCurrentSet(for cell:ExerciseCollectionViewCell) {
        // reps need to be > 0
        guard !(cell.repsTxt.text!.isEmpty) else { return }
        guard (Int16(cell.repsTxt.text!)! > 0) else {return}
        

        // save current set with the values in the Exercise cell fields
        newSet?.reps = Int16(cell.repsTxt.text!)!
        newSet?.weight = Int16(cell.weightTxt.text!) ?? 0
        newSet?.number = setNumber

    }
    
    func updateSetsCollectionView(){
        sets = exercise?.sets?.array as! Array<SetSerie>?
        setsCollectionView.reloadData()
    }
    
    func updateExercisesCollectionView(){
        exerciseCollectionView.reloadData()
    }
    
    func collectionViewCell(for subview: UIView) -> UICollectionViewCell? {
        var cell:UIView? = subview.superview
       
        while !(cell is UICollectionViewCell) && (cell != nil)  {
            cell = cell!.superview
        }
        
        return cell as? UICollectionViewCell
    }
    

   
    // MARK: UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        switch collectionView {
        case exerciseCollectionView:
            return workout.exercises?.count ?? 0
        case setsCollectionView:
            return exercise?.sets?.count ?? 0
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        if collectionView == exerciseCollectionView {
            let item =  collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell" , for: indexPath)  as! ExerciseCollectionViewCell
            item.setNumberLabel.text =  "Set " + setNumberAsString
            exerciseCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            return item
        } 
            if collectionView == setsCollectionView {
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as! SetCollectionViewCell
            
            let setAtIndexPath = sets?[indexPath.item]
            
            let setReps = (setAtIndexPath?.reps) ?? 0
            let setWeight = (setAtIndexPath?.weight) ?? 0
            let setNumber = (setAtIndexPath?.number) ?? 0
            
            item.setNumber.text = String(setNumber)
            item.reps.text = String(setReps)
            item.weight.text = String(setWeight)
            
            setsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            return item
        }
        let item = UICollectionViewCell()
        return item
    }

    
    // MARK: UITextFieldDelegate
    
    // dismis keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let cell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
            //cell.isSelected = true
            //print(" Is cell selected ? \(cell.isSelected)")
            switch textField {
            case cell.exerciseNameTxt:
                exercise?.name = textField.text
            //case cell.repsTxt:
                
            //case weightTxt:
            default:
                break
            }
            
        }
    }
    
    //MARK: Touches
    // dismiss keyboard when view touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        
        // Do any additional setup after loading the view.
        createExercise()
        createSet()
        updateSetsCollectionView()
        updateExercisesCollectionView()
    }
   
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        saveContext()
        
        view.endEditing(true)   // dismiss keyboard
        
        if segue.identifier == "Save Workout" {
            // save current set with the values in the fields
            
            
            
           // let cellIndexPath = exerciseCollectionView.indexPathsForSelectedItems?.first
            //let cell = exerciseCollectionView.cellForItem(at: cellIndexPath!) as! ExerciseCollectionViewCell
            let cell = exerciseCollectionView.visibleCells.first as! ExerciseCollectionViewCell
            
            
            saveCurrentSet(for: cell)

            if let destinationVC = segue.destination as? WorkoutTVC {
                destinationVC.workout = workout
                destinationVC.context = context
            }
        }
    }
    
    
    // MARK: UICollectionViewDelegate

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItem")
        //      self.selectedItemIndexPath = indexPath
    }
  
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
