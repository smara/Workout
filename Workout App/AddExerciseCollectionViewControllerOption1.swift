//
//  AddExerciseCollectionViewController.swift
//  Workout App
//
//  Created by Silvia Florido on 11/22/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "Cell"

class AddExerciseCollectionViewControllerOption1: UICollectionViewController {

   
    
    @IBOutlet weak var exerciseCollectionView: UICollectionView!
    
    var context: NSManagedObjectContext!   // yes, crash if nil
    var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
    
    var selectedExercise: Exercise?
    
    
    var selectedSet: SetSerie? //{ get { return setsCollectionView?.indexPathsForSelectedItems?.first }}    // for the visible exercise
    
    // for Collection Views, these arrays reflect the sets in DB for easy access using indexes
    var setsArray: Array<SetSerie>? { get { return selectedExercise?.sets?.array as! Array<SetSerie>? }   }
    var exercisesArray: Array<Exercise>? { get { return  workout?.exercises?.array as! Array<Exercise>? }   }
    
    
    //MARK: Actions
    
    @IBAction func addAnotherSetButton(_ sender: UIButton) {
        if let cell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
            
            // reps need to be > 0
            //  guard !(cell.repsTxt.text!.isEmpty) && (Int16(cell.repsTxt.text!)! > 0) else { return }
            
            cell.isSelected = true  // select the ExerciseCell because you just cliked in its button
            
            if selectedSet != nil {
                saveCurrentSet(selectedSet!, for: cell)
            } else {
                saveCurrentSet(setsArray!.last!, for: cell)
            }
            
            // get the exercise for this cell
            let cellIndexPath = exerciseCollectionView.indexPath(for: cell)!
            let exercise = exercisesArray![cellIndexPath.item]
            
            // create a new set for this exercise
            if createSet(for: exercise) != nil {
                exerciseCollectionView.reloadData()
                cell.setsCollectionView.reloadData()
                
                // select new set cell, the set itself is being seleted in createSet func
                //cell.setsCollectionView.selectItem(at: IndexPath(item: (setsArray!.index(of: newSet)!), section: 0) , animated: true, scrollPosition: .centeredHorizontally)
                
            } else {
                //error message
            }
        }
    }
    
    
    @IBAction func AddNextExerciseButton(_ sender: UIButton) {
        if let cell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
            
            if selectedSet != nil {
                saveCurrentSet(selectedSet!, for: cell)
            } else {
                saveCurrentSet((setsArray?.last)!, for: cell)
            }
            
            if let newExercise = createExercise() {   // creates new Set automatically
                
                exerciseCollectionView.reloadData()
                cell.setsCollectionView.reloadData()
                
                // scroll
                let indexPathForNextCell = IndexPath(item: (exercisesArray?.index(of: newExercise))!, section: 0)
                exerciseCollectionView.scrollToItem(at: indexPathForNextCell, at: .left, animated: false)
                
                
                // do I need to reload the new exercise setsCV ?
                
                /*
                 if let nextExCell = exerciseCollectionView.cellForItem(at: indexPathForNextCell ) as? ExerciseCollectionViewCell {
                 nextExCell.setsCollectionView?.selectItem(at: IndexPath(item: ((setsArray?.count)! - 1), section: 0) , animated: true, scrollPosition: .centeredHorizontally)
                 }*/
                
            } else {
                //error message
            }
            saveContext()
        }
    }
    
    
    func createExercise() -> Exercise? {
        if let exercise = Exercise.createExercise(inManagedObjectContext: context!) {
            workout.addToExercises(exercise)
            selectedExercise = exercise
            
            _ = createSet(for: exercise)
            
            print("Exercise created!")
            print(" Workout contains : \(workout.exercises?.count) exercises")
            
            return exercise
        }
        return nil
    }
    
    func createSet(for exercise:Exercise) -> SetSerie?{
        if let newSet = SetSerie.createSet(inManagedObjectContext: context) {
            exercise.addToSets(newSet)
            selectedSet = newSet
            
            newSet.number = Int16(setsArray!.count)
            newSet.reps = 5 // initial values to make UI more friendly
            newSet.weight = 5
            
            
            print("exercise has x sets: \(exercise.sets?.count)")
            return newSet
        }
        return nil
    }
    
    
    func saveCurrentSet(_ set: SetSerie , for cell: ExerciseCollectionViewCell){
        
        // save current set with the editable values in the Exercise cell fields
        
        // reps need to be > 0
        guard !(cell.repsTxt.text!.isEmpty) else { return }
        //guard (Int16(exerciseCell.repsTxt.text!)! > 0) else {return}
        
        set.reps = Int16(cell.repsTxt.text!)!
        set.weight = Int16(cell.weightTxt.text!) ?? 0
        
        
        
    }
    
    
    func collectionViewCell(for aView: UIView) -> UICollectionViewCell? {
        var cell:UIView? = aView.superview
        
        while !(cell is UICollectionViewCell) && (cell != nil)  {
            cell = cell!.superview
        }
        
        return cell as? UICollectionViewCell
    }
    /*
     func setsCollectionView(for exerciseCell: UICollectionViewCell) -> UICollectionView {
     var setsCollectionView: UICollectionView!
     
     if let exerciseCell = exerciseCell as? ExerciseCollectionViewCell {
     setsCollectionView = exerciseCell.setsCollectionView
     }
     return setsCollectionView
     }
     */
    func saveContext(){
        context.perform{
            do {
                try self.context.save()
                print("Exercise Saved.")
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    
    
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var items: Int = 0
        
        // Exercises Collection View
        if collectionView == exerciseCollectionView {
            items = workout.exercises?.count ?? 1
            
        } else {
            // Sets Collection View   --->   collectionView == setsCollectionView
            
            // pointer to to exerciseCell which this setsCV belongs to
            if let exerciseCell = collectionViewCell(for: collectionView) as? ExerciseCollectionViewCell {
                
                // get the exercise for this exerciseCell
                let indexPathForExerciseCell = exerciseCollectionView.indexPath(for: exerciseCell)
                let exerciseForThisCell = exercisesArray![indexPathForExerciseCell!.item]
                
                if let setsForThisExercise = exerciseForThisCell.sets?.array.count {
                    items = setsForThisExercise > 0 ? setsForThisExercise : 1
                }
                
            }
        }
        return items
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        
        
        // Exercises Collection View
        if collectionView == exerciseCollectionView {
            
            if let exerciseCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell" , for: indexPath)  as? ExerciseCollectionViewCell {
                
                // Populate cell
                if (setsArray!.count) > indexPath.item {
                    let set = selectedSet ?? (setsArray!.last!)
                    
                    exerciseCell.setNumberLabel.text =  "Set " + String(set.number)
                    exerciseCell.repsTxt.text = String(set.reps)
                    exerciseCell.weightTxt.text = String(set.weight)
                }
                
                cell = exerciseCell
            }
        } else {    //  Sets Collection View
            
            // pointer to to exerciseCell which this setsCV belongs to
            if let exerciseCell = collectionViewCell(for: collectionView) as? ExerciseCollectionViewCell {
                
                // get the exercise for this exerciseCell
                let indexPathForExerciseCell = exerciseCollectionView.indexPath(for: exerciseCell)
                let exerciseForThisCell = exercisesArray![indexPathForExerciseCell!.item]
                
                // if I have sets for this exercise, get them
                if let setsForThisExercise = exerciseForThisCell.sets?.array as? Array<SetSerie> {
                    if (setsForThisExercise.count) > indexPath.item {
                        
                        let set = setsForThisExercise[indexPath.item]
                        
                        if let item = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as? SetCollectionViewCell {
                            
                            item.setNumber.text = String(set.number)
                            item.reps.text = String((set.reps))
                            item.weight.text = String(set.weight)
                            
                            cell = item
                            
                        }
                    }
                    
                }
                
            }
        }
        return cell
    }
    
    
    
    
    // MARK: UITextFieldDelegate
    
    // dismis keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let cell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
            cell.isSelected = true
            
            switch textField {
            case cell.exerciseNameTxt:
                selectedExercise?.name = textField.text
                //case cell.repsTxt:
                
            //case weightTxt:
            default:
                break
            }
        }
    }
    
    
    
    
    //MARK: Touches
    // dismiss keyboard when view touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        
        // Do any additional setup after loading the view.
        
        _ = createExercise()  // creates the first Set automatically
        exerciseCollectionView.reloadData()
        // cell?.setsCollectionView.reloadData()    // check to see if it is going to be reload auto with the ExCV reload   ??????
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        saveContext()
        
        view.endEditing(true)   // dismiss keyboard
        
        if segue.identifier == "Save Workout" {
            // save current set with the values in the fields
            
            
            let cell = collectionViewCell(for: sender as! UIView) as! ExerciseCollectionViewCell
            // let cellIndexPath = exerciseCollectionView.indexPathsForSelectedItems?.first
            //let cell = exerciseCollectionView.cellForItem(at: cellIndexPath!) as! ExerciseCollectionViewCell
            //let cell = exerciseCollectionView.visibleCells.first as! ExerciseCollectionViewCell
            
            
            //let index = exerciseCollectionView.indexPath(for: cell)!
            //let currentExercise = exercisesArray![index.item]
            let setsCollectionView = cell.setsCollectionView
            let currentSet = setsArray![(setsCollectionView?.indexPathsForSelectedItems?.first)!.item]
            saveCurrentSet(currentSet, for: cell)
            //saveCurrentSet(for: cell, exercise: currentExercise)
            if let destinationVC = segue.destination as? WorkoutTVC {
                destinationVC.workout = workout
                destinationVC.context = context
            }
        }
    }
    
    
    // MARK: UICollectionViewDelegate
    
    /*
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
     if let collection = scrollView as? UICollectionView {
     if collection == exerciseCollectionView {
     collection.reloadData()
     }
     }
     }
     */
    
    /*
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
     //      self.selectedItemIndexPath = indexPath
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
    
}






