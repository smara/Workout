//
//  AddExerciseViewController.swift
//  Workout App
//
//  Created by Silvia Florido on 10/31/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData


class AddExerciseViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var exerciseNameTxt: UITextField!
    @IBOutlet weak var repsTxt: UITextField!
    @IBOutlet weak var weightTxt: UITextField!
    @IBOutlet weak var setNumberLabel: UILabel!
    
    @IBOutlet weak var setsCollectionView: UICollectionView!
    
    
    var context: NSManagedObjectContext!   // yes, crash if nil
    
    var exercise: Exercise?
    var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
    var newSet: SetSerie?
    
    var setNumber: Int16 = 0
    var setNumberAsString: String {   get { return String(setNumber)}   }
    var repsNumber: Int16 {   get { return Int16(repsTxt.text!) ?? 0}   }
    var weightNumber: Int16 {    get { return Int16(weightTxt.text!) ?? 0 }    }
    
    var sets: Array<SetSerie>?   // for Collection View

    
    
    //MARK: Actions
    
    @IBAction func addAnotherSetButton(_ sender: UIButton) {
        
        guard !(repsTxt.text!.isEmpty) else { return }
        
        // save current set with the values in the fields
        newSet?.reps = repsNumber
        newSet?.weight = weightNumber
        newSet?.number = setNumber
        
        // create a new set
        createSet()
        updateSetsCollectionView()
       
        
        // zero the set fields
        repsTxt.text = ""
        weightTxt.text = ""
        setNumberLabel.text = "Set " + setNumberAsString
        
    }
    
    func createExercise(){
        exercise = Exercise.createExercise(inManagedObjectContext: context!)
        workout.addToExercises(exercise!)
        print("Exercise created! Workout contains \(workout.exercises?.count) exercises")
    }
    
    func createSet(){
        if let newSet = SetSerie.createSet(inManagedObjectContext: context) {
            self.newSet = newSet
            setNumber += 1
            newSet.number = setNumber
            exercise?.addToSets(newSet)
        }
    }
    
    func save(){
        context.perform{
            do {
                try self.context.save()
                print("Exercise Saved.")
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    
    func updateSetsCollectionView(){
        let unsortedSets = exercise?.sets?.array as! Array<SetSerie>?
        sets = unsortedSets?.sorted(by: {$0.number < $1.number })
        setsCollectionView.reloadData()
        
        
    }

    
    
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as! SetCollectionViewCell
        let setAtIndexPath = sets?[indexPath.item]
        
        let setReps = (setAtIndexPath?.reps) ?? 0
        let setWeight = (setAtIndexPath?.weight) ?? 0
        let setNumber = (setAtIndexPath?.number) ?? 0
        
        item.setNumber.text = String(setNumber)
        item.reps.text = String(setReps)
        item.weight.text = String(setWeight)
    
        setsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)

        return item
    }
    
    
    
    // MARK: UITextFieldDelegate
    
    // dismis keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case exerciseNameTxt :
            exercise?.name = textField.text
        //case repsTxt:
       // break
        //case weightTxt:
        default:
            break
        }
    }
    
    //MARK: Touches
    // dismiss keyboard when view touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    
    // MARK: VC Life Cycle
    // in prepareForSegue add the Exercise to the Workout
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        createExercise()
        createSet()
        updateSetsCollectionView()
    }

   
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        save()
        
        view.endEditing(true)   // dismiss keyboard
        
        if segue.identifier == "Finish Workout" {
           // save current set with the values in the fields
            newSet?.reps = repsNumber
            newSet?.weight = weightNumber
            newSet?.number = setNumber
            save()
            
            if let destinationVC = segue.destination as? WorkoutTVC {
                destinationVC.workout = workout
                destinationVC.context = context
            }
        }
    }
    
    /*
     override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
     }
     */
    
}






















