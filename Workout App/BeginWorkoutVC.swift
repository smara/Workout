//
//  BeginWorkoutVC.swift
//  Workout App
//
//  Created by Silvia Florido on 1/25/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

class BeginWorkoutVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UITextFieldDelegate {
 

    @IBOutlet weak var exerciseCollectionView: UICollectionView!
    
   // var context: NSManagedObjectContext!   // yes, crash if nil
    var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
    
    var editionMode: Bool = false
    var selectedSet: SetSerie?
    
    var exercisesArray: Array<Exercise>? { get { return  workout?.exercises?.array as! Array<Exercise>? }   }
    
    var exerciseForThisCell: Exercise!
    
    lazy var tvc: SearchTableView = SearchTableView()
    
    
    
    //MARK: - Actions
    /*  Creating / Editing Features
    @IBAction func addAnotherSetButton(_ sender: UIButton) {
        if let exerciseCell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
            
            view.endEditing(true)
            // reps need to be > 0
            //  guard !(cell.repsTxt.text!.isEmpty) && (Int16(cell.repsTxt.text!)! > 0) else { return }
            
            exerciseCell.isSelected = true  // select the ExerciseCell because you just cliked on its button
            
            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
            exerciseForThisCell = exercisesArray![exerciseIndex]
            var setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
            
            
            // save current set, selected or last
            if selectedSet != nil {
                saveCurrentSet(selectedSet!, for: exerciseCell)
            } else {
                saveCurrentSet(setsForThisExercise.last!, for: exerciseCell)
            }
            
            
            // create a new set for this exercise
            if let newSet = createSet(for: exerciseForThisCell) {
                
                print("Added another Set: \(newSet.number) for Exercise: \(newSet.exercise?.name)")
                print("")
                
                
                // Use last Set data for ease of typing. This method will never be called for first set.
                setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
                let previousSet = setsForThisExercise[setsForThisExercise.count - 2]
                
                newSet.reps = previousSet.reps
                newSet.weight = previousSet.weight
                
                exerciseCell.setsCollectionView.reloadData()
                
                // scroll to show newSet
                let newSetIndexPath = IndexPath(item: (setsForThisExercise.index(of: newSet))!, section: 0)
                exerciseCell.setsCollectionView.scrollToItem(at: newSetIndexPath, at: .left, animated: true)
                
                //selects last set cell (appearance/highlighting defined in cell subclass)
                let lastSetIndex = IndexPath(item: (setsForThisExercise.count - 1), section: 0)
                exerciseCell.setsCollectionView.selectItem(at:  lastSetIndex, animated: true, scrollPosition: .left)
                
                //select set object
                selectedSet = newSet
                
            } else {
                //error message
            }
        }
    }
    
    
    @IBAction func AddNextExerciseButton(_ sender: UIButton) {
        if let exerciseCell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
            
            // move this block to the first else below if it works
            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
            exerciseForThisCell = exercisesArray![exerciseIndex]
            
            let lastSetForThisExercise = exerciseForThisCell.sets?.array.last as! SetSerie
            
            
            if selectedSet != nil {
                saveCurrentSet(selectedSet!, for: exerciseCell)
            } else {
                saveCurrentSet(lastSetForThisExercise, for: exerciseCell)
            }
            
            
            if let newExercise = createExercise() {   // creates new Set automatically
                exerciseCollectionView.reloadData()
                
                let indexPath = IndexPath(item: (exercisesArray?.index(of: newExercise))!, section: 0)
                exerciseCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            } else {
                //error message
            }
            //saveContext()
        }
    }
    
    
    func createExercise() -> Exercise? {
        if let exercise = Exercise.createExercise(inManagedObjectContext: context!) {
            
            workout.add(exercise: exercise)
            //workout.addToExercises(exercise)
            //selectedExercise = exercise
            exercise.number = Int16(exercisesArray!.count)
            exercise.name = "A " + String(exercise.number) + " - "
            _ = createSet(for: exercise)
            
            
            return exercise
        }
        return nil
    }
    
    
    func createSet(for exercise:Exercise) -> SetSerie?{
        if let newSet = SetSerie.createSet(inManagedObjectContext: context) {
            
            exercise.add(setSerie: newSet)
            //exercise.addToSets(newSet)
            newSet.number = Int16(exercise.sets!.count)
            
            // initial values for the FIRST Set only to make UI more friendly. Next Sets will use previouos set values.
            if newSet.number == 1 {
                newSet.reps = 10
                newSet.weight = 10
            }
            
            
            
            return newSet
        }
        return nil
    }
    
    
    */
    
    func saveCurrentSet(_ set: SetSerie , for cell: ExerciseCollectionViewCell){
        // save current set with the editable values in the Exercise cell fields
        
        // reps need to be > 0
        guard !(cell.repsTxt.text!.isEmpty) else { return }
        //guard (Int16(exerciseCell.repsTxt.text!)! > 0) else {return}
        
        set.reps = Int16(cell.repsTxt.text!)!
        set.weight = Int16(cell.weightTxt.text!) ?? 0
    }
    
    
    
    
    func collectionViewCell(for aView: UIView) -> UICollectionViewCell? {
        var cell:UIView? = aView.superview
        
        while !(cell is UICollectionViewCell) && (cell != nil)  {
            cell = cell!.superview
        }
        return cell as? UICollectionViewCell
    }
    
    
    
    /*   Editing Features
    func saveContext(){
        context.perform{
            do {
                try self.context.save()
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    */
    
    
    
    // MARK: - UITextFieldDelegate
    
    // dismis keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /*   Editing Features (Workout Name and Autocomplete)
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let exerciseCell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
            
            if textField == exerciseCell.exerciseNameLabel {
                let searchText: NSString = NSString(string:textField.text!)
                let resultingString = searchText.replacingCharacters(in: range, with: string)
                
                
                
                // TODO: Make a better blur effect under the search table.
                /*  let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
                 
                 
                 effectView.frame = view.frame
                 effectView.frame.origin =  CGPoint(x: textField.frame.origin.x, y: (textField.frame.origin.y + textField.frame.size.height))
                 
                 //effectView.alpha = 0.6
                 exerciseCell.contentView.addSubview(effectView)
                 */
                
                // Search Table View
                tvc.searchAutoCompleteEntries(with: resultingString, for: textField)
                
                exerciseCell.contentView.addSubview(tvc.tableView)
                
                //effectView.addSubview(tvc.tableView)
                tvc.txtColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
            }
        }
        return true
    }
    */
    
    /*  Editing Features (edit reps/weight/name)
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let exerciseCell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
            
            // update correct exercise and sets for THIS cell
            let exerciseIndex = (exerciseCollectionView.indexPath(for: exerciseCell)?.item)!
            exerciseForThisCell = exercisesArray![exerciseIndex]
            let setsForThisExercise = exerciseForThisCell.sets?.array as! Array<SetSerie>
            
            let setNumberString = exerciseCell.setNumberLabel.text!.replacingOccurrences(of: "Set ", with: "")
            let setNumberInt = Int(setNumberString)! - 1
            let set = setsForThisExercise[setNumberInt]
            
            
            // selects the correct set that is being updated
            selectedSet = set
            
            
            let index = IndexPath(item: setNumberInt, section: 0)
            
            
            // TODO: make numbers be updated to the Sets when typed in TxtFields (maybe use textFieldShouldChange func)
            
            switch textField {
            case exerciseCell.exerciseNameLabel:
                exerciseForThisCell.name = textField.text
                
            case exerciseCell.repsTxt:
                if exerciseCell.repsTxt.text == nil || exerciseCell.repsTxt.text == "0" || exerciseCell.repsTxt.text == "" {
                    exerciseCell.repsTxt.text = "1"
                }
                set.reps = Int16(exerciseCell.repsTxt.text!)!
                exerciseCell.setsCollectionView.reloadItems(at: [index])
            case exerciseCell.weightTxt:
                if exerciseCell.weightTxt.text == nil || exerciseCell.weightTxt.text == "" {
                    exerciseCell.weightTxt.text = "0"
                }
                set.weight = Int16(exerciseCell.weightTxt.text!)!
                exerciseCell.setsCollectionView.reloadItems(at: [index])
            default:
                break
            }
        }
    }
    
    */
    
    
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        var items: Int = 0
        
        // Exercises Collection View
        if collectionView == exerciseCollectionView {
            items = workout.exercises?.count ?? 1
            
        } else {
        // Sets Collection View   --->   collectionView == setsCollectionView
        
            if let setsForThisExercise = exerciseForThisCell.sets?.array.count {
                items = (setsForThisExercise > 0) ? setsForThisExercise : 1
            }
        }
        return items
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell!
        
        // Exercises Collection View
        if collectionView == exerciseCollectionView {
            
            // dequeue cell
            if let exerciseCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell" , for: indexPath)  as? ExerciseCollectionViewCell {
                
                // var to help mount and update Set cells
                exerciseForThisCell = exercisesArray![indexPath.item]
                
                
                // if I have sets for this exercise, get them
                if let setsForThisExercise = exerciseForThisCell.sets?.array as? Array<SetSerie> {
                    
                    // show selectedSet if any or last set
                    let set = selectedSet ?? (setsForThisExercise.last!)
                    
                    
                    // Populate Exercise cell
                    
                    // names
                    exerciseCell.exerciseNameLabel.text = exerciseForThisCell.name
                    
                    let exerciseNumber = exercisesArray!.index(of: exerciseForThisCell)!
                    
                    
                    switch exerciseNumber {
                    case exercisesArray!.startIndex, exercisesArray!.endIndex:
                        break
                    case exercisesArray!.startIndex:
                        exerciseCell.doneImageView.isHidden = true
                        exerciseCell.previousExerciseLabel.isHidden = true
                        exerciseCell.nextExerciseLabel.text = exercisesArray![exerciseNumber + 1].name
                        
                    case exercisesArray!.endIndex - 1:
                        exerciseCell.previousExerciseLabel.text = exercisesArray![exerciseNumber - 1].name
                        exerciseCell.nextExerciseLabel.isHidden = true
                        
                    default:
                        exerciseCell.previousExerciseLabel.isHidden = false
                        exerciseCell.nextExerciseLabel.isHidden = false
                        exerciseCell.doneImageView.isHidden = false
                        exerciseCell.previousExerciseLabel.text = exercisesArray![exerciseNumber - 1].name
                        exerciseCell.nextExerciseLabel.text = exercisesArray![exerciseNumber + 1].name
                    }
                    
                    // goal
                    
                    // need to create a func to check if reps and weights are equal for all sets or just say "mixed" in the goal text
                    
                    var allReps = Set<Int16>()
                    var allWeight = Set<Int16>()
                    
                    for setSerie in setsForThisExercise {
                        allReps.insert(setSerie.reps)
                        allWeight.insert(setSerie.weight)
                    }
                    
                    let repsNumber = allReps.count == 1 ? String(allReps.first!): "Mixed"
                    let weightNumber = allWeight.count == 1 ? String(allWeight.first!) : "Mixed"
                    
                    exerciseCell.goalLabel.text = "Goal: \(setsForThisExercise.count) Sets - \(repsNumber) Reps / \(weightNumber) Weight"
                    
                    // sets values
                    
                    //exerciseCell.setNumberLabel.text = String(set.number)
                    exerciseCell.repsTxt.text = String(set.reps)
                    exerciseCell.weightTxt.text = String(set.weight)
                }
                
                /*
                // show AddExercise button only for the last exercise cell
                if exerciseForThisCell == exercisesArray?.last {
                    exerciseCell.addExerciseButton.isHidden = false
                } else {
                    exerciseCell.addExerciseButton.isHidden = true
                }
                */
                cell = exerciseCell
            }
        } else {
            
            //  Sets Collection View
            
            if let setCell = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as? SetCollectionViewCell {
                
                // pointer to to exerciseCell which this setsCV belongs to
                if let exerciseCell = collectionViewCell(for: collectionView) as? ExerciseCollectionViewCell {
                    
                    // if I have sets for this exercise, get them
                    if let setsForThisExercise = exerciseForThisCell.sets?.array as? Array<SetSerie> {
                        
                        if (setsForThisExercise.count) > indexPath.item {
                            
                            let set = setsForThisExercise[indexPath.item]
                            
                            // Populate Set cell
                            setCell.setNumber.text = String(set.number)
                            setCell.reps.text = String((set.reps))
                            setCell.weight.text = String(set.weight)
                            
                            
                            // first cell wasn't created by addAnotherSet and was not selected.
                            if setsForThisExercise.count == 1 {
                                setCell.isSelected = true
                                selectedSet = set
                            }
                            
                            
                            // TODO: Improve highlighting appearence, maybe using a background image with shadow or gradient
                            // cell appearance
                            if selectedSet == set {
                                let views = setCell.subviews[0].subviews
                                for view in views {
                                    if let label = view as? UILabel {
                                        label.textColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
                                    }
                                }
                            } else {
                                let views = setCell.subviews[0].subviews
                                for view in views {
                                    if let label = view as? UILabel {
                                        label.textColor = UIColor(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
                                    }
                                }
                            }
                            
                            
                            // update the set number in the Exercise cell because when we createAnotherSet we don't reload the ExercisesCV
                            let setNumber = selectedSet ?? (setsForThisExercise.last!)
                            exerciseCell.setNumberLabel.text = "Set \(String(setNumber.number)) of \(String(setsForThisExercise.count))"
                            exerciseCell.setNumberLabel.textColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
                            
                            
                            cell = setCell
                        }
                    }
                }
            }
            print("Return Set Cell")
        }
        return cell
    }
    
    
    
    
    
    // MARK: - UICollectionViewDelegate
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard collectionView != exerciseCollectionView else { return }
        
        // get the right exercise for this cell
        let exerciseCell = collectionViewCell(for: collectionView) as! ExerciseCollectionViewCell
        let exerciseIndex = exerciseCollectionView.indexPath(for: exerciseCell)!
        exerciseForThisCell = exercisesArray?[exerciseIndex.item]
        
        let sets = exerciseForThisCell.sets?.array as! Array<SetSerie>
        selectedSet = sets[indexPath.item]
        
        exerciseCollectionView.reloadItems(at:[exerciseCollectionView.indexPath(for: exerciseCell)!])
    }
    
    
    
     
    
       
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        saveContext()
        
        view.endEditing(true)   // dismiss keyboard
        
        if segue.identifier == "Save Workout" {
            
            if let destinationVC = segue.destination as? WorkoutTVC {
                destinationVC.workout = workout
                destinationVC.context = context
            }
        }
    }
    */
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
               
        
        exerciseCollectionView.allowsMultipleSelection = false
        exerciseCollectionView.reloadData()
        
        
    }
    
    
    
    
    

}
