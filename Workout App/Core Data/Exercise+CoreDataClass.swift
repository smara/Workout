//
//  Exercise+CoreDataClass.swift
//  Workout App
//
//  Created by Silvia Florido on 10/30/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import Foundation
import CoreData

@objc(Exercise)
public class Exercise: NSManagedObject {
    
    
    
    class func createExercise(inManagedObjectContext context:NSManagedObjectContext) -> Exercise? {
        let exercise = NSEntityDescription.insertNewObject(forEntityName: "Exercise", into: context) as? Exercise
        return exercise
    }

    class func createExercise(inManagedObjectContext context:NSManagedObjectContext, in workout: Workout) -> Exercise? {
        if let exercise = NSEntityDescription.insertNewObject(forEntityName: "Exercise", into: context) as? Exercise {
        workout.add(exercise: exercise)
        return exercise
        }
        return nil
    }

    
    func add(setSerie: SetSerie) {
        let mutableSetSeries = sets?.mutableCopy() as! NSMutableOrderedSet
        mutableSetSeries.add(setSerie)
        sets = mutableSetSeries.copy() as? NSOrderedSet
    }
    
    
    func remove(setSerie: SetSerie) {
        let mutableSetSeries = sets?.mutableCopy() as! NSMutableOrderedSet
        mutableSetSeries.remove(setSerie)
        sets = mutableSetSeries.copy() as? NSOrderedSet    
    }
    
}
