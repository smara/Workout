//
//  Exercise+CoreDataProperties.swift
//  Workout App
//
//  Created by Silvia Florido on 2/19/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import Foundation
import CoreData


extension Exercise {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exercise> {
        return NSFetchRequest<Exercise>(entityName: "Exercise");
    }

    @NSManaged public var done: Bool
    @NSManaged public var name: String?
    @NSManaged public var number: Int16
    @NSManaged public var sets: NSOrderedSet?
    @NSManaged public var workout: Workout?

}

// MARK: Generated accessors for sets
extension Exercise {

    @objc(insertObject:inSetsAtIndex:)
    @NSManaged public func insertIntoSets(_ value: SetSerie, at idx: Int)

    @objc(removeObjectFromSetsAtIndex:)
    @NSManaged public func removeFromSets(at idx: Int)

    @objc(insertSets:atIndexes:)
    @NSManaged public func insertIntoSets(_ values: [SetSerie], at indexes: NSIndexSet)

    @objc(removeSetsAtIndexes:)
    @NSManaged public func removeFromSets(at indexes: NSIndexSet)

    @objc(replaceObjectInSetsAtIndex:withObject:)
    @NSManaged public func replaceSets(at idx: Int, with value: SetSerie)

    @objc(replaceSetsAtIndexes:withSets:)
    @NSManaged public func replaceSets(at indexes: NSIndexSet, with values: [SetSerie])

    @objc(addSetsObject:)
    @NSManaged public func addToSets(_ value: SetSerie)

    @objc(removeSetsObject:)
    @NSManaged public func removeFromSets(_ value: SetSerie)

    @objc(addSets:)
    @NSManaged public func addToSets(_ values: NSOrderedSet)

    @objc(removeSets:)
    @NSManaged public func removeFromSets(_ values: NSOrderedSet)

}
