//
//  SetSerie+CoreDataClass.swift
//  Workout App
//
//  Created by Silvia Florido on 10/30/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import Foundation
import CoreData

@objc(SetSerie)
public class SetSerie: NSManagedObject {
    
    class func createSet(inManagedObjectContext context:NSManagedObjectContext) -> SetSerie? {
        return NSEntityDescription.insertNewObject(forEntityName: "SetSerie", into: context) as? SetSerie
    }
    
    class func createSet(inManagedObjectContext context:NSManagedObjectContext, for exercise: Exercise) -> SetSerie? {
        if let set = NSEntityDescription.insertNewObject(forEntityName: "SetSerie", into: context) as? SetSerie {
            exercise.addToSets(set)
            return set
        }
        return nil
    }

}
