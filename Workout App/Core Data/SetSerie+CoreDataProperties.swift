//
//  SetSerie+CoreDataProperties.swift
//  Workout App
//
//  Created by Silvia Florido on 2/19/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import Foundation
import CoreData


extension SetSerie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SetSerie> {
        return NSFetchRequest<SetSerie>(entityName: "SetSerie");
    }

    @NSManaged public var done: Bool
    @NSManaged public var number: Int16
    @NSManaged public var reps: Int16
    @NSManaged public var weight: Int16
    @NSManaged public var exercise: Exercise?

}
