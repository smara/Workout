//
//  Workout+CoreDataClass.swift
//  Workout App
//
//  Created by Silvia Florido on 10/30/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import Foundation
import CoreData

@objc(Workout)
public class Workout: NSManagedObject {
    
    class func createWorkout(inManagedObjectContext context:NSManagedObjectContext) -> Workout{
        let workout = NSEntityDescription.insertNewObject(forEntityName: "Workout", into: context) as! Workout
        return workout
    }
    
    
    
}
