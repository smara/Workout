//
//  CreateWorkoutViewController.swift
//  Workout App
//
//  Created by Silvia Florido on 10/26/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

// TODO:
/** Development notes

    Validate name when textField didEndEditing and show error OR thinking about presenting a modal view asking the user if it really wants to keep the repeated name.  Oct 26, 2016
 
    Decided to colect all data and create the workout after Next is pressed to avoid problems with unique name DB checking and autosaving to soon. Oct 26, 2016
 
    - workout name field - size and validation - show icon to fast-delete textfield (x) like the Apple Contacts App
    - scroll fields to visible area when keyboard is visible
    - actions - save / next / more options
    - autocomplete/search name. Show list from the begining to help the user choose a pré-existing name.
 

**/

import UIKit
import CoreData


class CreateWorkoutViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var workoutNameTxt: UITextField!
    @IBOutlet weak var frequenceSegments: MultiSelectSegmentedControl!
    @IBOutlet weak var nameWarningLabel: UILabel!
    
   // var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    var context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    var workout: Workout! // yes, crash if nil
    
    // before creation in DB this is false. After creation, this VC enters in editionMode and will not create a new workout, it will just update data.
    var isEditingMode: Bool = false
    
    var frequence: String {
        get {
            let frequenceArray = frequenceSegments.selectedSegmentTitles as! Array<String>
            return frequenceArray.joined(separator: ",")
        }
    }
    
    lazy var tvc: SearchTableView = SearchTableView()

    
    
    func isWorkoutNameValid() -> Bool {
        guard let newName =  workoutNameTxt.text?.trimmingCharacters(in: CharacterSet.whitespaces) else {  return false  }
        guard newName.isEmpty == false else {
            // show message saying name is empty
            return false
        }
        
        // TODO:  validate if name exists in DB
        // Maybe replace this animation with a modal view asking if we should keep the name even though it is not unique.
        
        //guard newName not in DB already (isNameUnique) else {
        UIView.animate(withDuration: 1,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: {self.nameWarningLabel.alpha = 1},
                       completion: { (Bool) -> Void in
                        UIView.animate(withDuration: 1,
                                       delay: 2,
                                       options: .curveEaseIn,
                                       animations: {self.nameWarningLabel.alpha = 0},
                                       completion: nil)
        })
        print("Já existe um treino com esse nome! Por favor escolha outro.")
        
        // return false }
        
        return true
    }

    
    func createWorkoutInDB(){
        if !isEditingMode {
            workout = Workout.createWorkout(inManagedObjectContext: context)
            /*
             context.perform{
             do {
             try self.context.save()
             print("Workout Saved.")
             } catch let error {
             print("Core Data Saving Error: ", error)
             }
             }*/
            isEditingMode = true
        }
        workout.name = workoutNameTxt.text!   //  TODO: Fix or establish this. See Text and Segment methods
        workout.frequence = frequence
        workout.isActive = true
        workout.isTemplate = true
        
    
        let duration = [["start":todaysDate(),"end":nil]]
        workout.periodOfActivity = duration as NSObject
        // let today = workout.periodOfActivity as! Dictionary<String,String?>
        //print("Date test: \(today) --------------")
    }
    
    func todaysDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        let today = formatter.string(from: Date())
        return today
    }
    
    
    
    // MARK: UITextFieldDelegate
    
    // dismis keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    // dismiss keyboard when view touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    // dismiss keyboard and call textFieldDidEndEditing for name validation
    @IBAction func selectedSegmentedDidChange(_ sender: MultiSelectSegmentedControl) {
        view.endEditing(true)
       // guard workout.frequence != frequence else { return }
        if workout != nil {        // TODO: FIX THIS Workout shouldn't be nil. ?? Or keep workout being created only at the end and set name and frequence with createWorkoutInDB
            workout.frequence = frequence
        }
    }

    // validates name after textFieldEndEditing
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == workoutNameTxt {
           // guard textField.text != workout.name else { return }
            guard isWorkoutNameValid() else { return }
            guard workout != nil else { return }   // TODO: FIX THIS Workout shouldn't be nil. ?? Or keep workout being created only at the end ?
            workout.name = textField.text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText: NSString = NSString(string:textField.text!)
        let resultingString = searchText.replacingCharacters(in: range, with: string)
        
        // TODO: Make a better blur effect under the search table.
        /*  let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
         
         
         effectView.frame = view.frame
         effectView.frame.origin =  CGPoint(x: textField.frame.origin.x, y: (textField.frame.origin.y + textField.frame.size.height))
         
         //effectView.alpha = 0.6
         exerciseCell.contentView.addSubview(effectView)
         */
        
        // Search Table View
        tvc.searchAutoCompleteEntries(with: resultingString, for: textField)
        
        view.addSubview(tvc.tableView)
        
        //effectView.addSubview(tvc.tableView)
        tvc.txtColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
        return true
    }
    
    
    
    // MARK: - VC Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let countVC = self.navigationController?.viewControllers.count {
            guard countVC > 1 else {return}
            let previousVC = self.navigationController?.viewControllers[countVC - 2]
            
            if (previousVC as? TodayViewController) != nil {
               self.navigationItem.setHidesBackButton(true, animated: false)
            }             
            
        }
        print("\(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))")

    }

    

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        view.endEditing(true)   // dismiss keyboard
        if isWorkoutNameValid(){
            createWorkoutInDB()     //create workout Obj
            
            
            if segue.identifier == "Add Exercise"{
                let destinationVC = segue.destination as! AddExerciseCVC
                destinationVC.workout = workout
              //  destinationVC.context = context
                // destinationVC.editionMode = self.editionMode
            
            } else if segue.identifier == "show muscular groups" {
                
                
            } else if segue.identifier == "show split options" {
                let destinationVC = segue.destination as! SplitRoutineVC
                destinationVC.context = context
            }
        }
    }
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        // validate that workout has a name at least, frequence may be optional
        //get name and check if not empty and unique in DB, if so:
        if identifier == "Add Exercise" {
            return isWorkoutNameValid()
        }
        else {
            return true
        }

    }
    
    
    
    @IBAction func unwindFromAdvancedOptions(segue: UIStoryboardSegue) {
    
    
        
        if segue.source as? SplitRoutineVC == nil {
            segue.source.dismiss(animated: true, completion: nil)
        }
    }
    

}








