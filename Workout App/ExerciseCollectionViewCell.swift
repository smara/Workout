//
//  ExerciseCollectionViewCell.swift
//  Workout App
//
//  Created by Silvia Florido on 11/5/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit


class ExerciseCollectionViewCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
 
    // Create Workout Sequence
    @IBOutlet weak var exerciseNameTxt: UITextField!
    @IBOutlet weak var repsTxt: UITextField!
    @IBOutlet weak var weightTxt: UITextField!
    @IBOutlet weak var setNumberLabel: UILabel!
    
    @IBOutlet weak var addToSetButton: UIButton!
    @IBOutlet weak var addExerciseButton: UIButton!
    @IBOutlet weak var setsCollectionView: UICollectionView! {
        didSet {
            setsCollectionView.delegate = self
            setsCollectionView.dataSource = self
            setsCollectionView.isPagingEnabled = true
        }
    }
    
    
    
    // Begin Workout Sequence
    @IBOutlet weak var exerciseNameLabel: UILabel!
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var nextExerciseLabel: UILabel!
    @IBOutlet weak var previousExerciseLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var doneImageView: UIImageView!
    
    
    let cellID = "setCell"
//    var workout: Workout?
    var exercise: Exercise!
//    var sets: Array<SetSerie> = { return exercise?.sets?.array as! Array<SetSerie>}

    
    lazy var tvc: SearchTableView = SearchTableView()
    
    // MARK: Override methods
    override func prepareForReuse() {
        super.prepareForReuse()
        setsCollectionView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        contentView.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    
    
//    // MARK: Actions and helpers
//    private func updateExerciseValuesBasedOn(set: SetSerie) {
//        setNumberLabel.text = String(set.number)
//        weightTxt.text = String(set.weight)
//        repsTxt.text = String(set.reps)
//    }
//    
   
    
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return exercise?.sets?.count ?? 0
        //return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let setCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! SetCollectionViewCell
        
        // populate cell
        let set = exercise.sets?.array[indexPath.item] as! SetSerie    //sets[indexPath.item]
        setCell.setNumber.text = String(set.number)
        setCell.reps.text = String((set.reps))
        setCell.weight.text = String(set.weight)
        
        return setCell
    }
    
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // update exercise cell to display the selected set
        if let set = exercise.sets?.array[indexPath.item] as? SetSerie {
            setNumberLabel.text = "Set \(set.number)"
            repsTxt.text = String(set.reps)
            weightTxt.text = String(set.weight)
        }
    }

    
    
    
    
    
    // MARK: TextFieldDelegate
    // dismiss keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case exerciseNameTxt:
            let initialText =  NSString(string: textField.text!)  //NSString(string: textField.text!) {
            let searchText = initialText.replacingCharacters(in: range, with: string)
            
                tvc.searchAutoCompleteEntries(with: searchText, for: textField)
                contentView.addSubview(tvc.tableView)
                tvc.txtColor = UIColor(red: 51/255, green: 132/255, blue: 255/255, alpha: 1)
            
            return true
        case repsTxt, weightTxt:
            if (Int(string) != nil) {
                return true
            } else {
                return false
            }
        default:
            return true
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        case exerciseNameTxt:
            exercise.name = textField.text
        case repsTxt:
            if repsTxt.text == nil || repsTxt.text == "0" || repsTxt.text == "" {
                repsTxt.text = "1"
            }
            let selectedSet =  exercise.sets!.array[setsCollectionView.indexPathsForSelectedItems![0].item] as! SetSerie
            selectedSet.reps = Int16(repsTxt.text!)!
        case weightTxt:
            if repsTxt.text == nil || repsTxt.text == "" {
                repsTxt.text = "0"
            }
            let selectedSet =  exercise.sets!.array[setsCollectionView.indexPathsForSelectedItems![0].item] as! SetSerie
            selectedSet.weight = Int16(weightTxt.text!)!
        default:
            break
        }
    }
    
    
    
    // MARK: Actions
    @IBAction func addAnotherSetButton(_ sender: UIButton) {
        self.endEditing(true)
        
        // save current set data - done in textfield delegates
        // create new set in model
        if let newSet = SetSerie.createSet(inManagedObjectContext: exercise.managedObjectContext!, for: exercise) {
            setup(newSet: newSet)
            
            
            // add to Sets Collection
            let indexPath =  IndexPath(item: setsCollectionView.numberOfItems(inSection: 0), section: 0)
            setsCollectionView.insertItems(at: [indexPath])
            
            // selects new set and scroll to it
            setsCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
            
            // update exercise data
            setNumberLabel.text = "Set \(newSet.number)"
            repsTxt.text = String(newSet.reps)
            weightTxt.text = String(newSet.weight)
        }
        
    }
    
     
    
    // MARK: private methods
    private func setup(newSet: SetSerie) {
            newSet.number = Int16(exercise.sets!.count)
        
                // Use last Set data for ease of typing if not the 1st set
                let previousSet = exercise.sets!.array[newSet.number - 2] as! SetSerie
                newSet.reps = previousSet.reps
                newSet.weight = previousSet.weight
    }
    
    
    
    
    
    
}

































