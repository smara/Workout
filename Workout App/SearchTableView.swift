//
//  SearchTableView.swift
//  Workout App
//
//  Created by Silvia Florido on 12/7/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import Foundation


class SearchTableView: UITableViewController {

    var txtField: UITextField!
    
    var txtColor: UIColor = UIColor.black
    
    var workoutNames = ["Lower Body", "Upper Body", "Legs Day", "Abs", "Chest", "Triceps", "Biceps",  "Back", "Shoulders"]
    
    var exerciseNames = ["Squat","Leg Press","Deadlift","Leg Extension","Leg Curl","Snatch","Standing Calf Raise","Seated Calf Raise","Bench Press","Chest Fly","Lats Pulldown","Lats Pull-up","Bent-over Row","Upright Row","Shoulder Press","Military Press","Lateral Raise","Triceps Pushdown","Triceps Extension","Biceps Curl","Hammer Curl","Crunch", "Leg Raise", "Back Extension", "Good Morning"]
    
    fileprivate lazy var searchArray:[String] = {
        var baseArray = [""]
        let viewCalling = self.txtField.superview?.superview
        
        if viewCalling is UICollectionViewCell {
            baseArray = ["Supino","Flexões","Remada","Rosca","Triceps pulley","Press", "Agachamento","Cadeira Extensora","Leg Press", "Cadeira Flexora", "Mesa Flexora", "Mesa Flexora", "Agachamento Smith", "Extensão", "Flexão Vertical","Flexão Horizontal","Adução","Abdução","Avanço", "Glúteos", "Gêmeos", "Afundo", "Stiff", "Glúteos 4 apoios", "Levantamento Terra", "Desenvolvimento Máquina","Elevação Lateral", "Remada Alta","Deltóide Posterior", "Encolhimento", "Depressão Ombro", "Deenvolvimento Halteres", "Desenvolvimento Barra", "Ëlevação Frontal", "Rosca Inversa", "Flexão Punho", "Extensão Punho", "Supino Inclinado", "Supino Declinado", "Peck Deck", "Dumbbell Press", "Crucifixo","Pull-Over","Cross-Over","Flexão  Braço", "Fly","Pulley Costas","Pulley Frente","Remada Curvada","Hiperextensão","Dorsais Máquina","Barra Fixa","Pull Side","Pull Down","Lombar","Remada Pulley","Cavalinho","Crucifixo Inverso","Rosca Direta","Rosca Scott","Rosca Alternada","Rosca Simultânea","Rosca Concentrada","Rosca Martelo","Triceps Corda","Triceps Testa","Triceps Pulley","Triceps Francês","Mergulho","Coice","Pulley Invertido","Abdominal Reto","Elevação Pernas","Oblíquo Alternado","Abdominal Reto no Cabo","Infra","Air Bike","Bola","Prancha","Remador" ]
            //var coreDataArray = {Workout.allWorkoutNamesInContext(self.context!)}
            //baseArray = baseArray.filter{!(coreDataArray()!.contains($0))}
            print("lazy init searchArray: \(baseArray)")
        } else {
            baseArray = ["Treino de Pernas","Treino de Braços","Abdominais","Treino de Glúteos", "Membros Superiores", "Membros Inferiores"]
        
        }
        return baseArray
    }()
    
    var filteredSearchArray:[String]?
    

    func searchAutoCompleteEntries(with string:String, for textField:UITextField) {
        
        self.txtField = textField
        
        
        let predicate = NSPredicate(format: "self contains[c] %@", string)
        filteredSearchArray = searchArray.filter { predicate.evaluate(with: $0) }
        //print("String: \(string)")
       // print("searchArray after filter: \(searchArray)")
        //print("filteredSearchArray: \(filteredSearchArray)")
        
        if !(filteredSearchArray!.isEmpty){
            autoCompleteTableView.isHidden = false
        }else{
            autoCompleteTableView.isHidden = true
        }
        
        autoCompleteTableView.frame.size.height = (autoCompleteTableView.rowHeight * CGFloat(self.filteredSearchArray!.count))
        autoCompleteTableView.reloadData()
        
    }
    
    
    
    
    
    // MARK: Table View
    // TODO: TableView is out of its superview (view1) bounds so it's not receiving touches out of view1 bounds
    // redesign UI to remove all those views
    lazy var autoCompleteTableView: UITableView = {
        //let autoCompleteTableView: UITableView = self.tableView
        self.tableView.frame.origin = CGPoint(x: self.txtField.frame.origin.x, y: (self.txtField.frame.origin.y + self.txtField.frame.size.height))
        self.tableView.frame.size.width = self.txtField.frame.size.width
        self.tableView.rowHeight = self.txtField.frame.size.height
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = true
        self.tableView.separatorStyle = .none
      //  self.txtField.superview?.addSubview(autoCompleteTableView)
        return self.tableView
        
    }()
    
    
    // MARK: Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.txtField.text = filteredSearchArray![(indexPath as NSIndexPath).row]
        autoCompleteTableView.isHidden = true
     //   view.endEditing(true)
    }
    
    
    // MARK: Table View Data Source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("number of rows in section: \(filteredSearchArray?.count)")
        return filteredSearchArray!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = filteredSearchArray![(indexPath as NSIndexPath).row]
        cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 15.0)
        cell.textLabel?.textColor = txtColor
        return cell
    }
    


    

}

