//
//  SetCollectionViewCell.swift
//  Workout App
//
//  Created by Silvia Florido on 11/4/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit

class SetCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var setNumber: UILabel!
    @IBOutlet weak var reps: UILabel!
    @IBOutlet weak var weight: UILabel!
    
    
    
    override var isSelected: Bool {
        didSet {
            selectedCellApearance(isSelected: isSelected)
        }
    }
    
    
    private func selectedCellApearance(isSelected: Bool) {
        let selectedColor = UIColor(red: 51/255.0, green: 132/255.0, blue: 255/255.0, alpha: 1.0)
        let unselectedColor = UIColor(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
        
        let color = isSelected ? selectedColor : unselectedColor
        
        let views = subviews[0].subviews
        for view in views {
            if let label = view as? UILabel {
                label.textColor = color
            }
        }
    }
    

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        selectedCellApearance(isSelected: isSelected)
    }
    
}
            
