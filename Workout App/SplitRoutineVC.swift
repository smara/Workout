//
//  SplitRoutineVC.swift
//  Workout App
//
//  Created by Silvia Florido on 2/21/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

class SplitRoutineVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var workoutText: UITextField!
    @IBOutlet weak var workoutsTableView: UITableView!
    
   // @IBOutlet weak var titleLabel: UILabel!
    //@IBOutlet weak var subTitleLabel: UILabel!
    
    
    var context: NSManagedObjectContext! {
        didSet {
            workouts = getWorkouts()
        }
    }
    
    var workouts: [Workout]?
    
    func getWorkouts() -> [Workout]?{
        // TODO : exclude inactive workouts
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
        //request.predicate = NSPredicate(format: "frequence CONTAINS %@", Date().shortWeekdayName)
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))]

        do {
            workouts = try self.context.fetch(request) as? [Workout]
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        
        return workouts
    }
    
    
    // MARK: Table View DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workouts?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Split Workouts", for: indexPath)
        
        //already guarded by the instruction " workouts?.count ?? 0 " in the function above, if we got to this point is because workouts.count isn't nil
        //guard workouts != nil else { return cell }
        
        cell.textLabel?.text = workouts![indexPath.row].name
        
        if let exercises = workouts![indexPath.row].exercises?.array as? Array<Exercise> {
            var setsCount = 0
            
            for exercise in exercises {
                if let sets = exercise.sets {
                    setsCount += sets.count
                }
            }
            
            cell.detailTextLabel?.text = "\(exercises.count) Exercises, \(setsCount) Sets"
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        workoutText.text = workouts![indexPath.row].name
        UIView.animate(withDuration: 1, delay: 0.0, options: .curveEaseInOut, animations: {
            self.workoutsTableView.alpha = 0.0
        }, completion: nil)
    }
    
    // MARK: TextField Delegate
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 1, delay: 0.0, options: .curveEaseInOut, animations: {
            self.workoutsTableView.alpha = 1.0
        }, completion: nil)
        
        return true
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
