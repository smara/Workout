//
//  Support.swift
//  Workout App
//
//  Created by Silvia Florido on 11/22/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import Foundation
/*
if let exerciseCell = exerciseCollectionView.visibleCells.first as? ExerciseCollectionViewCell {
    //  if let unwrappedExCell = exerciseCell {
    // if let setsCollectionView = unwrappedExCell.setsCollectionView {
    
    //   let howManySetsForThisExercise =  exercises?[indexPath.item].sets?.count //exercise?.sets?.count
    
    // sets for that exercise cell
    
    if (sets!.count) > indexPath.item {     //if there are sets to show
        if let set = sets?[indexPath.item] {
            if let item = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as? SetCollectionViewCell {
*/


/*
 //
 //  AddExerciseAllTogetherCellCollectionVC.swift
 //  Workout App
 //
 //  Created by Silvia Florido on 11/20/16.
 //  Copyright © 2016 Silvia Florido. All rights reserved.
 //
 
 import UIKit
 import CoreData
 
 private let reuseIdentifier = "Cell"
 
 class AddExerciseAllTogetherCellCollectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
 
 @IBOutlet weak var exerciseCollectionView: UICollectionView!
 //@IBOutlet weak var setsCollectionView: UICollectionView!
 
 var context: NSManagedObjectContext!   // yes, crash if nil
 var workout: Workout!  // yes, crash if nil. Nil workout at this point is a big problem.
 
 var exercise: Exercise?
 var newSet: SetSerie?
 
 var setNumber: Int16 = 0
 var setNumberAsString: String {   get { return String(setNumber)}   }
 
 var sets: Array<SetSerie>?   // for Collection View
 var exercises: Array<Exercise>?
 
 //var selectedItemIndexPath: IndexPath?
 
 //var visibleExerciseCellIndex: IndexPath?
 
 //MARK: Actions
 
 @IBAction func addAnotherSetButton(_ sender: UIButton) {
 if let cell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
 cell.isSelected = true
 
 // reps need to be > 0
 guard !(cell.repsTxt.text!.isEmpty) && (Int16(cell.repsTxt.text!)! > 0) else { return }
 
 saveCurrentSet(for: cell)
 
 // zero the set fields
 cell.repsTxt.text = ""
 cell.weightTxt.text = ""
 cell.setNumberLabel.text = "Set " + setNumberAsString
 
 // create a new set
 createSet()
 updateExercisesCollectionView()
 updateSetsCollectionView(for: cell)
 
 }
 
 }
 
 @IBAction func AddNextExerciseButton(_ sender: UIButton) {
 if let cell = collectionViewCell(for: sender) as? ExerciseCollectionViewCell {
 cell.isSelected = true
 
 saveCurrentSet(for: cell)
 
 setNumber = 0
 createExercise()
 createSet()
 //exerciseCollectionView.reloadItems(at: <#T##[IndexPath]#>)
 
 
 updateExercisesCollectionView()
 sets = exercise?.sets?.allObjects as! Array<SetSerie>?
 exercises = workout?.exercises?.allObjects as! Array<Exercise>?
 //  print(sets)
 cell.setsCollectionView.reloadData()
 //updateSetsCollectionView(for: cell)
 
 }
 }
 
 
 func createExercise(){
 exercise = Exercise.createExercise(inManagedObjectContext: context!)
 
 workout.addToExercises(exercise!)
 
 print("Exercise created!")
 print(" Workout contains : \(workout.exercises?.count) exercises")
 
 
 }
 
 func createSet(){
 if let newSet = SetSerie.createSet(inManagedObjectContext: context) {
 self.newSet = newSet
 setNumber += 1
 newSet.number = setNumber
 newSet.reps = 5
 newSet.weight = 5
 exercise?.addToSets(newSet)
 print("exercise has x sets: \(exercise?.sets?.count)")
 
 
 }
 }
 
 func saveContext(){
 context.perform{
 do {
 try self.context.save()
 print("Exercise Saved.")
 } catch let error {
 print("Core Data Saving Error: ", error)
 }
 }
 }
 
 func saveCurrentSet(for cell:ExerciseCollectionViewCell) {
 // reps need to be > 0
 guard !(cell.repsTxt.text!.isEmpty) else { return }
 guard (Int16(cell.repsTxt.text!)! > 0) else {return}
 
 
 // save current set with the values in the Exercise cell fields
 newSet?.reps = Int16(cell.repsTxt.text!)!
 newSet?.weight = Int16(cell.weightTxt.text!) ?? 0
 newSet?.number = setNumber
 
 }
 
 func updateSetsCollectionView(for cell:ExerciseCollectionViewCell?){
 sets = exercise?.sets?.allObjects as! Array<SetSerie>?
 //  print(sets)
 cell?.setsCollectionView.reloadData()
 
 
 }
 
 
 func updateExercisesCollectionView(){
 exercises = workout.exercises?.allObjects as! Array<Exercise>?
 exerciseCollectionView.reloadData()
 }
 
 func collectionViewCell(for aView: UIView) -> UICollectionViewCell? {
 var cell:UIView? = aView.superview
 
 while !(cell is UICollectionViewCell) && (cell != nil)  {
 cell = cell!.superview
 }
 
 return cell as? UICollectionViewCell
 }
 
 
 
 // MARK: UICollectionViewDataSource
 
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 var items: Int = 0
 
 // Exercises Collection View
 if collectionView == exerciseCollectionView {
 print("Exercises :  \(workout.exercises?.count)")
 items = workout.exercises?.count ?? 0
 }
 
 // Sets Collection View
 var setsCollectionView: UICollectionView?
 if let cell = exerciseCollectionView.visibleCells.first as? ExerciseCollectionViewCell {
 setsCollectionView = cell.setsCollectionView
 if collectionView == setsCollectionView{
 print("Sets :  \(exercise?.sets?.count)")
 items = exercise?.sets?.count ?? 0
 }
 }
 
 return items
 }
 
 
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 var cell: UICollectionViewCell!
 
 
 
 // Exercises Collection View
 if collectionView == exerciseCollectionView {
 if let exerciseCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell" , for: indexPath)  as? ExerciseCollectionViewCell {
 
 
 exerciseCell.setNumberLabel.text =  "Set " + setNumberAsString
 
 if (sets!.count) > indexPath.item {     //if there are sets to show
 if let set = sets?[indexPath.item] {
 
 exerciseCell.repsTxt.text = String((set.reps))
 exerciseCell.weightTxt.text = String(set.weight)
 }
 }
 /*
 if  (exercises?[indexPath.item].sets?.count)! > 0 {
 for set in sets! {
 if let indexInt = sets?.index(of: set) {
 let indexPathForThisSet = IndexPath(item: indexInt, section: 0)
 if let item = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as? SetCollectionViewCell {
 
 
 item.setNumber.text = String(set.number)
 item.reps.text = String((set.reps))
 item.weight.text = String(set.weight)
 
 }
 }
 
 }
 
 }
 */
 exerciseCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
 cell = exerciseCell
 //return item
 }}
 
 
 
 // Sets Collection View
 if let exerciseCell = exerciseCollectionView.visibleCells.first as? ExerciseCollectionViewCell {
 if collectionView == exerciseCell.setsCollectionView {
 
 //   let howManySetsForThisExercise =  exercises?[indexPath.item].sets?.count //exercise?.sets?.count
 
 if (sets!.count) > indexPath.item {     //if there are sets to show
 if let set = sets?[indexPath.item] {
 if let item = collectionView.dequeueReusableCell(withReuseIdentifier: "setCell" , for: indexPath) as? SetCollectionViewCell {
 
 
 
 /// test
 
 let index = IndexPath(item: 0, section: 0)
 let exCell = exerciseCollectionView.cellForItem(at: index) as? ExerciseCollectionViewCell
 let setCollection = exCell?.setsCollectionView
 
 let setCell = setCollection?.cellForItem(at: IndexPath(item: 0, section: 0)) as? SetCollectionViewCell
 setCell?.reps.text = "oi"
 
 
 /// test
 
 
 item.setNumber.text = String(set.number)
 item.reps.text = String((set.reps))
 item.weight.text = String(set.weight)
 
 
 collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
 //return item
 cell = item
 
 }
 }
 }
 
 }
 }
 // let item = UICollectionViewCell()
 return cell
 }
 
 
 // MARK: UITextFieldDelegate
 
 // dismis keyboard
 func textFieldShouldReturn(_ textField: UITextField) -> Bool {
 textField.resignFirstResponder()
 return true
 }
 
 func textFieldDidEndEditing(_ textField: UITextField) {
 
 if let cell = collectionViewCell(for: textField) as? ExerciseCollectionViewCell {
 cell.isSelected = true
 //print(" Is cell selected ? \(cell.isSelected)")
 switch textField {
 case cell.exerciseNameTxt:
 exercise?.name = textField.text
 //case cell.repsTxt:
 
 //case weightTxt:
 default:
 break
 }
 
 }
 }
 
 //MARK: Touches
 // dismiss keyboard when view touched
 override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
 view.endEditing(true)
 super.touchesBegan(touches, with: event)
 }
 
 
 
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 // Uncomment the following line to preserve selection between presentations
 // self.clearsSelectionOnViewWillAppear = false
 
 // Register cell classes
 
 // Do any additional setup after loading the view.
 createExercise()
 createSet()
 updateExercisesCollectionView()
 updateSetsCollectionView(for: nil)
 
 }
 
 
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 saveContext()
 
 view.endEditing(true)   // dismiss keyboard
 
 if segue.identifier == "Save Workout" {
 // save current set with the values in the fields
 
 
 
 // let cellIndexPath = exerciseCollectionView.indexPathsForSelectedItems?.first
 //let cell = exerciseCollectionView.cellForItem(at: cellIndexPath!) as! ExerciseCollectionViewCell
 let cell = exerciseCollectionView.visibleCells.first as! ExerciseCollectionViewCell
 
 
 saveCurrentSet(for: cell)
 
 if let destinationVC = segue.destination as? WorkoutTVC {
 destinationVC.workout = workout
 destinationVC.context = context
 }
 }
 }
 
 
 
 
 
 
 // MARK: UICollectionViewDelegate
 
 func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
 if let collection = scrollView as? UICollectionView {
 if collection == exerciseCollectionView {
 collection.reloadData()
 }
 
 
 }
 }
 
 
 /*
 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 
 //      self.selectedItemIndexPath = indexPath
 }
 */
 
 /*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */
 
 /*
 // Uncomment this method to specify if the specified item should be selected
 override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */
 
 /*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
 
 }
 */
 
 
 }

 */






