//
//  thisWeekWorkoutsCell.swift
//  Workout App
//
//  Created by Silvia Florido on 12/15/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit

class ThisWeekWorkoutsCell: UITableViewCell {

    
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var doneImageView: UIImageView!
    
    override func prepareForReuse() {
        weekdayLabel.text = ""
        workoutNameLabel.text = ""
        //doneImageView.image = ""
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
