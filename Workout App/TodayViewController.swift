    //
//  TodayViewController.swift
//  Workout App
//
//  Created by Silvia Florido on 12/14/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

class TodayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var todayWorkoutTable: UITableView!
    
    // optional label according workouts you have (or no workout)
    @IBOutlet weak var youHaveWorkoutsLabel: UILabel!
    @IBOutlet weak var selectWorkoutsLabel: UILabel!
    
    @IBOutlet weak var thisWeekLabel: UILabel!
    @IBOutlet weak var thisWeekWorkoutsTable: UITableView!
    
    
    @IBOutlet weak var todaysTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var todayTableTopWithDateConstraint: NSLayoutConstraint!
    
    
    var weekday: String {
        get {  return Date().weekdayName  }
    }
    
    var month: String {
        get {  return Date().monthName  }
    }
    
    var day: String {
        get {  return String(Date().dateLocalComponents.day!)  }
    }
    var week: String {
        get {
            let today = Date()
            let weekdayNumber = today.dateLocalComponents.weekday!
            
            let sunday = Calendar.current.date(byAdding: .day, value: (-weekdayNumber + 1), to: today)!
            let saturday = Calendar.current.date(byAdding: .day, value: (7 - weekdayNumber), to: today)!
            
            
            let sundayString = sunday.shortWeekdayName + ", " + sunday.monthName + " " + String(sunday.dateLocalComponents.day!)
            let saturdayString = saturday.shortWeekdayName + ", " + saturday.monthName + " " + String(saturday.dateLocalComponents.day!)
            
            return (sundayString + " - " + saturdayString)
        }
    }
    
    
    
    var todaysWorkout: [Workout]?
    
    
    func getWorkouts() -> [Workout]?{
        // TODO : exclude inactive workouts
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
        request.predicate = NSPredicate(format: "frequence CONTAINS %@", Date().shortWeekdayName)
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))]
        
        var results:[Workout]?
        do {
            results = try self.context.fetch(request) as? [Workout]
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        
        return results
    }
    
    
    
    // array of arrays, [0] = sunday workouts array, [1] = monday workouts array
    lazy var thisWeekWorkouts: [[Workout]] = {
        // TODO : exclude inactive workouts
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))]
        
        // NSSortDescriptor(key: "frequence", ascending: true, selector: #selector(NSString.CompareOptions)
        var results:[Workout]?
        do {
            results = try self.context.fetch(request) as? [Workout]
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        
        var thisWeek = [[Workout]]()
    
        // order by frequence
        if results != nil {
            
            let weekdays = Calendar.current.shortWeekdaySymbols
            
            for weekday in weekdays{
                let weekdayResults = results
                
                thisWeek.append(weekdayResults!.filter({$0.frequence!.contains(weekday)}))
            }
        }
        return thisWeek
    }()
    
    
    // MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == todayWorkoutTable {
        
        }
        
    }
    
    
    
    
    // MARK: - TableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == todayWorkoutTable{
            return 1
        } else if tableView == thisWeekWorkoutsTable {
            return thisWeekWorkouts.count
        }
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == thisWeekWorkoutsTable {
          //  if thisWeekWorkouts[section].count > 1 {
                return Calendar.current.weekdaySymbols[section]
            //}
           // return nil
        } else {
         return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        if #available(iOS 8.2, *) {
            header.textLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightLight)
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == todayWorkoutTable {
            if todaysWorkout != nil {
                
                // if todaysWorkout = 0 then return 1 because an alternate cell will be displayed offering to create a workout
                return todaysWorkout!.count > 0 ? todaysWorkout!.count : 0
            }
        } else if tableView == thisWeekWorkoutsTable {
            return thisWeekWorkouts[section].count
        }
        return 0
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        if tableView == todayWorkoutTable {
            
            guard todaysWorkout?.count != 0 else {
                let noWorkoutsCell = tableView.dequeueReusableCell(withIdentifier: "noWorkoutsCell")!
                return noWorkoutsCell
            }
            
            let todayWorkoutCell = tableView.dequeueReusableCell(withIdentifier: "todayCell", for: indexPath) as! TodayWorkoutCell
            let workout = todaysWorkout![indexPath.row]
            
            // mount the detail text
            let exercises =  workout.exercises?.array as! Array<Exercise>
            let exercisesCount = exercises.count
            var setsCount = 0
            for exercise in exercises {
                setsCount += exercise.sets!.count
            }
            todayWorkoutCell.exercisesSetsLabel.text = "\(exercisesCount) Exercises, \(setsCount) Sets"//"8 Exercises, 24 Sets - 1:15 hs"
            
            // mount frequence
            let frequence = workout.frequence
            todayWorkoutCell.frequenceLabel.text = frequence?.replacingOccurrences(of: ",", with: "/")
            
          //  todayWorkoutCell.backgroundView = UIImageView(image: UIImage(named: "todaysWorkoutCellBG"))
            todayWorkoutCell.workoutNameLabel.text = workout.name
            
            cell = todayWorkoutCell
            
            
            
        } else if tableView == thisWeekWorkoutsTable {
            let thisWeekWorkoutCell = tableView.dequeueReusableCell(withIdentifier: "thisWeekWorkoutCell", for: indexPath) as! ThisWeekWorkoutsCell
            
            let workout = thisWeekWorkouts[indexPath.section][indexPath.row] as Workout
           /* if thisWeekWorkouts[indexPath.section].count == 1 {
                thisWeekWorkoutCell.weekdayLabel.text = Calendar.current.weekdaySymbols[indexPath.section]
            } else {
                thisWeekWorkoutCell.weekdayLabel.text = ""
            }
            */
            thisWeekWorkoutCell.weekdayLabel.text = ""
            thisWeekWorkoutCell.workoutNameLabel.text = workout.name
            
            cell = thisWeekWorkoutCell
        }
        
        
        
        return cell
    }
    
    
    
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dateLabel.text = "\(weekday), \(month) \(day)"
        thisWeekLabel.text = week
//        let fontFamilyNames = UIFont.systemFont(ofSize: 10.0)//familyNames
      /*  print("\(fontFamilyNames)")
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }*/
        
        print("\(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))")

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        todaysWorkout = getWorkouts()

       // todaysWorkout = nil
        
        if todaysWorkout == nil || (todaysWorkout?.count)! < 1 {
            youHaveWorkoutsLabel.text = "You have no workouts planned for today."
            selectWorkoutsLabel.isHidden = true
            
            todayWorkoutTable.isHidden = true
            todaysTableHeight.constant = 0
            todayTableTopWithDateConstraint.constant = 40
         
        } else {
            youHaveWorkoutsLabel.text = "You have workouts planned for today."

            todayWorkoutTable.reloadData()
            thisWeekWorkoutsTable.reloadData()
            
            todayWorkoutTable.isHidden = false
            todayTableTopWithDateConstraint.constant = 60

            if todaysWorkout!.count < 4 {
                todaysTableHeight.constant = CGFloat(todaysWorkout!.count * 60)
            } else {
                todaysTableHeight.constant = 180
            }
            
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        /*
        if todaysWorkout!.count < 4 {
            todaysTableHeight.constant = CGFloat(todaysWorkout!.count * 60)
        } else {
            todaysTableHeight.constant = 180
        }*/
        
    }
    
   
    
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "show todays workout" {
            let destination = segue.destination as! WorkoutTVC
          
            let workout = todaysWorkout![todayWorkoutTable.indexPathForSelectedRow!.row]
            destination.workout = workout

            destination.context = context
        } else if segue.identifier == "show All Workouts" {
            let destination = segue.destination as! AllWorkoutsTVC
            
            destination.context = context
            
        }
     }
    
    
    // MARK: - TabBarDelegate
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //todaysWorkout = self.getWorkouts()
    }
    
    
    
    @IBAction func unwindFromCreateWorkout(segue: UIStoryboardSegue) {
    
    //
    
    }
   
    
}





























