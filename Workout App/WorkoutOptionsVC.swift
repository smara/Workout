//
//  WorkoutOptionsVC.swift
//  Workout App
//
//  Created by Silvia Florido on 2/19/17.
//  Copyright © 2017 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

class WorkoutOptionsVC: UIViewController {

    var workout: Workout!
    var context: NSManagedObjectContext!
    
    @IBOutlet weak var activeSwitch: UISwitch!
    
    @IBAction func activeSwitchValueChanged(_ sender: UISwitch) {
        var arrayOfPeriods = workout.periodOfActivity as! Array<Dictionary<String, String?>>
         let today = Date().todayMDY
        // activating workout
        if sender.isOn {
              arrayOfPeriods.append(["start":today,"end":nil])
            workout.periodOfActivity = arrayOfPeriods as! NSObject
        } else {    // DEactivating
            var lastActivation = arrayOfPeriods.last!
            lastActivation.updateValue(today, forKey: "end")
        
            arrayOfPeriods.removeLast()
            arrayOfPeriods.append(lastActivation)
            
            workout.periodOfActivity = arrayOfPeriods as! NSObject
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        // TODO: remove before deployment
        // to avoid crash on old models without isActive attribute
        if workout.isActive != nil {
            activeSwitch.isOn = workout.isActive
        } else {
            workout.isActive = false
            activeSwitch.isOn = false
        }
    }

  
}
