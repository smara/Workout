//
//  WorkoutTVC.swift
//  Workout App
//
//  Created by Silvia Florido on 11/3/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit
import CoreData

class WorkoutTVC: CoreDataTVC {
    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var exercisesResumeLable: UILabel!
    @IBOutlet weak var frequenceLabel: UILabel!
    @IBOutlet weak var congratsLabel: UILabel!
    
    @IBOutlet weak var beginButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    
    var workout: Workout! { didSet { context = workout.managedObjectContext }}  // yes, crash if nil. Nil workout at this point is a big problem.
    var context: NSManagedObjectContext! {  didSet {   initFetchedResultsController()  }  }
    
    //var frc: NSFetchedResultsController<NSFetchRequestResult>!
    
    
    
    
    func initFetchedResultsController(){
        
        let workoutName = workout.name!
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exercise")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        request.predicate = NSPredicate(format: "workout.name == %@ ", workoutName)
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                         managedObjectContext: context,
                                         sectionNameKeyPath: nil,
                                         cacheName: nil)
        
        
    }
    
    
    
    func setTableHeader(){
        var totalSetsCount: Int = 0
        workoutNameLabel.text = workout.name
        frequenceLabel.text = workout.frequence?.replacingOccurrences(of: ",", with: " / ")
        
        if let exercises = fetchedResultsController?.fetchedObjects as? [Exercise]{
            
            for exercise in exercises {
                totalSetsCount += exercise.sets!.count
            }
            
            exercisesResumeLable.text = "\(fetchedResultsController?.fetchedObjects!.count) Exercises, \(totalSetsCount) Sets"
        }
    }
    
    
    
    func saveContext() {
        context.perform{
            do {
                try self.context.save()
            } catch let error {
                print("Core Data Saving Error: ", error)
            }
        }
    }
    
   
    
    // MARK: - Table view data source
    
    // Other DataSource methods implemented in super class (CoreDataTVC)
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutCell", for: indexPath)
        
        guard let exercise = fetchedResultsController?.object(at: indexPath) as? Exercise else {
            fatalError("Unexpected Object in FetchedResultsController")
        }
        
        var repsString: String!
        var weightString: String!
        
        if let setsArray = exercise.sets?.array {
            
            var repsSet = Set<Int16>()
            var weightSet = Set<Int16>()
            
            for setSerie in setsArray {
                let set = setSerie as! SetSerie
                repsSet.insert(set.reps)
                weightSet.insert(set.weight)
            }
            
            if repsSet.count > 1{
                repsString = "mixed"
            } else {
                repsString = String(repsSet.first!)
                
            }
            
            if weightSet.count > 1 {
                weightString = "mixed"
            } else {
                weightString = String(weightSet.first!)
            }
        }
        
        
        let goal = "Goal: \(exercise.sets!.count) sets - \(repsString!) reps / \(weightString!) kgs"
        
        cell.textLabel?.text = exercise.name
        cell.detailTextLabel?.text = goal
        
        return cell
    }
    
    
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            // Delete the row from the data source
            
            //tableView.deleteRows(at: [indexPath], with: .fade)
            
            // Delete object in model and let fetchedResultsController sync the table
            let exerciseToDelete = fetchedResultsController?.object(at: indexPath) as! NSManagedObject
            
            context.delete(exerciseToDelete)
            saveContext()
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        
        
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    */
    
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableHeaderView = headerView
        setTableHeader()
        
        
        if let countVC = self.navigationController?.viewControllers.count {
            guard countVC > 1 else {return}
            let previousVC = self.navigationController?.viewControllers[countVC - 2]
            
            if (previousVC as? AddExerciseCVC) != nil {
                congratsLabel.isHidden = false
                navigationItem.setHidesBackButton(true, animated: false)
                
                if let index = navigationItem.rightBarButtonItems?.index(of: beginButton) {
                    navigationItem.rightBarButtonItems?.remove(at: index)
                }
                
            } else if (previousVC as? TodayViewController) != nil {
                
                if let index = navigationItem.rightBarButtonItems?.index(of: doneButton) {
                    navigationItem.rightBarButtonItems?.remove(at: index)
                }
            }
        }
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "Begin Workout" {
            let destination = segue.destination as! BeginWorkoutVC
            destination.workout = workout
            
        } else if segue.identifier == "Done Workout Creation" {
            //let destination = segue.destination as! TodayViewController
            
        } else if segue.identifier == "Show Workout Options" {
            let destination = segue.destination as! WorkoutOptionsVC
            destination.workout = workout
            destination.context = context
        }
        
    }
    
    
}




















