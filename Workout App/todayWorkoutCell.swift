//
//  todayWorkoutCell.swift
//  Workout App
//
//  Created by Silvia Florido on 12/15/16.
//  Copyright © 2016 Silvia Florido. All rights reserved.
//

import UIKit

class TodayWorkoutCell: UITableViewCell {

    
    
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var exercisesSetsLabel: UILabel!
    @IBOutlet weak var frequenceLabel: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
